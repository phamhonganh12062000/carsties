#!/bin/sh

echo "Building the image..."
docker pull $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest || true

if [ -z "${CI_COMMIT_TAG+x}" ]; then
  docker build \
    --cache-from "$CI_REGISTRY_IMAGE/$IMAGE_NAME:latest" \
    --file "$DOCKERFILE_NAME" \
    --tag "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHA" \
    --tag "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_REF_SLUG" \
    --tag "$CI_REGISTRY_IMAGE/$IMAGE_NAME:latest" .
else
  docker build \
    --cache-from "$CI_REGISTRY_IMAGE/$IMAGE_NAME:latest" \
    --file "$DOCKERFILE_NAME" \
    --tag "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHA" \
    --tag "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_TAG" \
    --tag "$CI_REGISTRY_IMAGE/$IMAGE_NAME:latest" .
fi

docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHA"

if [ ! -z "${CI_COMMIT_TAG+x}" ]; then
  docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_TAG"
fi

docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:latest"
