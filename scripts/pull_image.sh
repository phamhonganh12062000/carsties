#!/bin/sh

ssh -i $ID_RSA -o StrictHostKeyChecking=no $SERVER_USER@$SERVER_IP <<EOF
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    docker pull $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest || true
EOF
