## Background services

- Background tasks are handled by `BackgroundTaskQueue` (Enqueue & Dequeue) and `BackgroundTaskWorker` (Execution)
- Background task logic is implemented in `Services` folders and each logic is given an API endpoint

## Job scheduling
- (Plan B) Quartz package is used to handle job scheduling by registering jobs that invoke API endpoint with `IHttpClientFactory`
- Custom Cron Job execution in `Shared.Common/Jobs` project folder
  - How it works: Check for current active jobs -> Map the jobs to a hash table (DateTime as key - list of current runs of a specific job as value) -> Run the jobs -> Get the next batch of job runs
