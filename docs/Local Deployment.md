# Local deployment (Ubuntu)

[Link to the tutorial](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-continuous-deployment-pipeline-with-gitlab-on-ubuntu)

## Create user for deployment

```bash
sudo adduser deployer # Add new user
sudo usermod -aG docker deployer # Grant privileges to user equal to root user
su deployer # Switch to new user
```

## Add new SSH key

```bash
ssh-keygen -b 4096
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
cat ~/.ssh/id_rsa # Generate SSH private key
```

- Add the key as a private variable in GitLab CICD with this settings:

```
Key: ID_RSA
Value: Paste your SSH private key from your clipboard (including a line break at the end).
Type: File
Environment Scope: All (default)
Protect variable: Checked // NOTE: Uncheck this when testing on non-main branches
Mask variable: Unchecked
```

- Create a variable for your server's IP

```
Key: SERVER_IP
Value: your_server_IP
Type: Variable
Environment scope: All (default)
Protect variable: Checked // NOTE: Uncheck this when testing on non-main branches
Mask variable: Checked
```


> [!question] Why we use SSH commands on our own server if GitLab runner that executes the commands is the exact same server?
> As the runner executes the commands inside a Docker container, you would deploy inside the container instead of the server for a more sustainable and extensible solution e.g., migrating the app to a different server, using a different runner server, etc.
