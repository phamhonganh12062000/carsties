
[Docs](https://masstransit.io/documentation/concepts/consumers)

## Consumers

### What is a consumer?

- A class consuming one or more message types

### How things work

- Control flows from MassTransit to the developer's code in response to an event (delivery of a message by transport)
- When a message is delivered from the transport on a receive endpoint and the message is consumed, MassTransit creates a container scope > resolve a consumer instance > execute the implemented `Consume` method with the `ConsumeContext` containing the message.
- If the `Task` completes successfully, the message is acknowledged and removed from the queue.



> [!info] 
> While the consumer method is executing, the message is **unavailable** to other receiver endpoints.


## Faults

