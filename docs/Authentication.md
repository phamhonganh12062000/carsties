# Authentication

## NextAuth

### Components

- `routes.ts` includes providers + token handling methods
- `session/page.tsx` for developers to monitor sessions
- `signin/page.tsx` to display login on the front-end
- `next-auth.d.ts` for custom NextAuth interfaces
- `middleware.ts` to match endpoints between the front-end and the identity server on the back-end
- `authActions.ts` to process current user and token

### Flow

- Login button calls for identity server on the backend => Identity server handles the login process => Login data goes to `authActions.ts` with options from `routes.ts` => `authActions.ts` handles the current user and token => Current user's session is displayed inside `session/page.tsx`
