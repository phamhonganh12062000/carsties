# Project Setup

## Setup the solution

```powershell
# Check templates
dotnet new list

dotnet new sln
dotnet new webapi/classlib/xunit -o src/backend/SolutionName.ServiceName # Startup/Class library project

dotnet sln add path/to/csproj

# Add references between project
dotnet add /path/to/ProjectToReferFrom.csproj reference /path/to/ProjectToReferTo.csproj

```

- Run all the docker compose files: `docker compose -f docker-compose.deps.yml -f docker-compose.app.yml up -d`
