
## What is it?

- Google Remote Procedure Call
- HTTP/2 protocol to transport binary message (inc TLS)
- Focused on high performance
- Rely on Protocol buffer (contracts between services)
- Support multiple languages

![Communication between gPRC server (Auction) and gRPC Client (Bidding)]({88B75098-1C88-47EF-AD3F-2044932F0427}.png)
