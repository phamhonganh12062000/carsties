using System.Security.Claims;
using System.Text.Encodings.Web;
using Carsties.EmailService;
using Carsties.IdentityService.Provider.Models;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carsties.IdentityService.Provider.Pages.Register
{
  [SecurityHeaders]
  [AllowAnonymous]
  public class Index : PageModel
  {
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IEmailSenderFactory _emailSender;

    public Index(UserManager<ApplicationUser> userManager, IEmailSenderFactory emailSender)
    {
      _userManager = userManager;
      _emailSender = emailSender;
    }

    [BindProperty]
    public RegisterViewModel Input { get; set; }

    [BindProperty]
    public bool RegisterSuccess { get; set; }

    public IActionResult OnGet(string returnUrl)
    {
      Input = new RegisterViewModel
      {
        ReturnUrl = returnUrl,
      };

      return Page();
    }

    public async Task<IActionResult> OnPost()
    {
      if (Input.Button != "register")
      {
        return Redirect("~/");
      }

      if (!ModelState.IsValid)
      {
        return Page();
      }

      var user = new ApplicationUser
      {
        UserName = Input.Username,
        Email = Input.Email,
        EmailConfirmed = false,
      };

      var result = await _userManager.CreateAsync(user, Input.Password);

      if (!result.Succeeded)
      {
        foreach (var error in result.Errors)
        {
          ModelState.TryAddModelError(error.Code, error.Description);
        }

        return Page();
      }

      await _userManager.AddClaimsAsync(user, new Claim[]
      {
          new Claim(JwtClaimTypes.Name, Input.FullName)
      });

      RegisterSuccess = true;

      if (RegisterSuccess)
      {
        TempData["SuccessMessage"] = "Successfully registered - Please confirm your email.";
      }

      // await _userManager.SetTwoFactorEnabledAsync(user, true);
      await SendEmailConfirmationLink(user, Input.ReturnUrl);

      return Redirect(Input.ReturnUrl);
    }

    private async Task SendEmailConfirmationLink(ApplicationUser user, string returnUrl)
    {
      var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);

      var confirmationLink = Url.Page(
        "../ConfirmEmail/Index",
        pageHandler: null,
        values: new { userId = user.Id, token, returnUrl },
        protocol: Request.Scheme
      );

      var message = new Message(
        tos: new List<EmailAddress>() { new() { Address = user.Email, DisplayName = user.UserName } },
        subject: "Confirmation email link",
        content: $"Please confirm your email by <a href='{HtmlEncoder.Default.Encode(confirmationLink)}'>clicking here</a>.",
        attachments: null
      );

      await _emailSender.SendEmailAsync(message);
    }
  }
}
