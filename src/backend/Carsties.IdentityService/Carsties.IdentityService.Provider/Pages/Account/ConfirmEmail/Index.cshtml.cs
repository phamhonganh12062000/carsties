using Carsties.IdentityService.Provider.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carsties.IdentityService.Provider.Pages.ConfirmEmail
{
  [AllowAnonymous]
  public class Index : PageModel
  {
    private readonly UserManager<ApplicationUser> _userManager;

    public Index(UserManager<ApplicationUser> userManager)
    {
      _userManager = userManager;
    }

    public async Task<IActionResult> OnGetAsync(string userId, string token, string returnUrl)
    {
      ViewData["ReturnUrl"] = returnUrl;

      if (userId == null || token == null)
      {
        return Redirect("~/");
      }

      var user = await _userManager.FindByIdAsync(userId);

      if (user is null)
      {
        return NotFound($"Unable to load user with ID: {userId}");
      }

      var result = await _userManager.ConfirmEmailAsync(user, token);

      if (!result.Succeeded)
      {
        throw new InvalidOperationException($"Error confirming email for user with ID '{userId}':");
      }

      return Page();
    }
  }
}
