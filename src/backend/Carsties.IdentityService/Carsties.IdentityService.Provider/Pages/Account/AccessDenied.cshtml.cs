// Copyright (c) Duende Software. All rights reserved.
// See LICENSE in the project root for license information.

using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carsties.IdentityService.Provider.Pages.Account;

public class AccessDeniedModel : PageModel
{
    public void OnGet()
    {
    }
}
