using Carsties.EmailService;
using Carsties.IdentityService.Provider.Models;
using Carsties.Shared.Common.Logging;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carsties.IdentityService.Provider.Pages.TwoStepsVerification
{
  public class Index : PageModel
  {
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IEmailSenderFactory _emailSender;
    private readonly ILoggerManager _logger;

    public Index(UserManager<ApplicationUser> userManager, IEmailSenderFactory emailSender, SignInManager<ApplicationUser> signInManager, ILoggerManager logger)
    {
      _userManager = userManager;
      _emailSender = emailSender;
      _signInManager = signInManager;
      _logger = logger;
    }

    [BindProperty]
    public TwoStepsVerificationViewModel Input { get; set; }

    public bool RememberLogin { get; set; }

    public string ReturnUrl { get; set; }

    public async Task<IActionResult> OnGetAsync(string username, bool rememberLogin, string returnUrl)
    {
      var user = await _userManager.FindByNameAsync(username);

      if (user is null)
      {
        return RedirectToPage("../../Home/Error/Index", new { returnUrl });
      }

      var providers = await _userManager.GetValidTwoFactorProvidersAsync(user);

      if (!providers.Contains("Username"))
      {
        return RedirectToPage("../../Home/Error/Index", new { returnUrl });
      }

      await SendTwoFactorCode(user);

      ReturnUrl = returnUrl;
      RememberLogin = rememberLogin;

      return Page();
    }

    public async Task<IActionResult> OnPostAsync(bool rememberLogin, string returnUrl)
    {
      if (!ModelState.IsValid)
      {
        return Page();
      }

      returnUrl ??= Url.Content("~/");

      var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

      if (user is null)
      {
        return RedirectToPage("../../Home/Error/Index", new { returnUrl });
      }

      var result = await _signInManager.TwoFactorAuthenticatorSignInAsync(Input.TwoFactorCode, rememberLogin, Input.RememberLogin);

      if (result.Succeeded)
      {
        _logger.LogInfo("User with ID '{UserId}' logged in with 2fa.", user.Id);
        return LocalRedirect(returnUrl);
      }
      else if (result.IsLockedOut)
      {
        _logger.LogWarning("User with ID '{UserId}' account locked out.", user.Id);
        return RedirectToPage("../Lockout/Index");
      }
      else
      {
        _logger.LogWarning("Invalid authenticator code entered for user with ID '{UserId}'.", user.Id);
        ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
        return Page();
      }
    }

    private async Task SendTwoFactorCode(ApplicationUser user)
    {
      var token = await _userManager.GenerateTwoFactorTokenAsync(user, "Username");

      var message = new Message(
        tos: new List<EmailAddress>() { new() { Address = user.Email, DisplayName = user.UserName } },
        subject: "Authentication token",
        content: $"Your two-step verification code is: {token}",
        attachments: null
      );

      await _emailSender.SendEmailAsync(message);
    }
  }
}
