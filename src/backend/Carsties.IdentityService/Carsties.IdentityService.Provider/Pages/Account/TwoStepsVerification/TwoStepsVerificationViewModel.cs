﻿using System.ComponentModel.DataAnnotations;

namespace Carsties.IdentityService.Provider;

public class TwoStepsVerificationViewModel
{
  [Required]
  [DataType(DataType.Text)]
  public string TwoFactorCode { get; set; }
  public bool RememberLogin { get; set; }
}
