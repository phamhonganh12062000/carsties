using System.Text.Encodings.Web;
using Carsties.EmailService;
using Carsties.IdentityService.Provider.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carsties.IdentityService.Provider.Pages.ForgotPassword
{
  [AllowAnonymous]
  public class Index : PageModel
  {
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IEmailSenderFactory _emailSender;

    public Index(UserManager<ApplicationUser> userManager, IEmailSenderFactory emailSender)
    {
      _userManager = userManager;
      _emailSender = emailSender;
    }

    [BindProperty]
    public ForgotPasswordViewModel Input { get; set; }

    public async Task<IActionResult> OnPostAsync()
    {
      if (!ModelState.IsValid)
      {
        return Page();
      }

      var user = await _userManager.FindByEmailAsync(Input.Email);

      if (user is null || !await _userManager.IsEmailConfirmedAsync(user))
      {
        return RedirectToPage("../ForgotPasswordConfirmation/Index");
      }

      await SendResetPasswordLink(user);

      return RedirectToPage("../ForgotPasswordConfirmation/Index");
    }

    private async Task SendResetPasswordLink(ApplicationUser user)
    {
      var token = await _userManager.GeneratePasswordResetTokenAsync(user);

      var callbackUrl = Url.Page(
        "../ResetPassword/Index",
        pageHandler: null,
        values: new { token, user.Email },
        protocol: Request.Scheme
      );

      var message = new Message(
        tos: new List<EmailAddress>() { new() { Address = user.Email, DisplayName = user.UserName } },
        subject: "Reset password token",
        content: $"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.",
        attachments: null
      );

      await _emailSender.SendEmailAsync(message);
    }
  }
}
