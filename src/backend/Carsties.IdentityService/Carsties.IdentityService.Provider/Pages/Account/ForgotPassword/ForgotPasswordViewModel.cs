﻿using System.ComponentModel.DataAnnotations;

namespace Carsties.IdentityService.Provider;

public class ForgotPasswordViewModel
{
  [Required]
  [EmailAddress]
  public string Email { get; set; }
}
