using Carsties.IdentityService.Provider.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carsties.IdentityService.Provider.Pages.ResetPassword
{
  [AllowAnonymous]
  public class Index : PageModel
  {
    private readonly UserManager<ApplicationUser> _userManager;

    public Index(UserManager<ApplicationUser> userManager)
    {
      _userManager = userManager;
    }

    [BindProperty]
    public ResetPasswordViewModel Input { get; set; }

    public IActionResult OnGet(string token = null, string email = null)
    {
      if (token == null)
      {
        return BadRequest("A token must be supplied for password reset");
      }
      else if (email == null)
      {
        return BadRequest("An email must be supplied for password reset");
      }
      else
      {
        Input = new ResetPasswordViewModel
        {
          Token = token,
          Email = email,
        };

        return Page();
      }
    }

    public async Task<IActionResult> OnPostAsync()
    {
      if (!ModelState.IsValid)
      {
        return Page();
      }

      var user = await _userManager.FindByEmailAsync(Input.Email);

      if (user is null)
      {
        return RedirectToPage("../ResetPasswordConfirmation/Index");
      }

      var result = await _userManager.ResetPasswordAsync(user, Input.Token, Input.Password);

      if (result.Succeeded)
      {
        return RedirectToPage("../ResetPasswordConfirmation/Index");
      }

      foreach (var error in result.Errors)
      {
        ModelState.AddModelError(string.Empty, error.Description);
      }

      return Page();
    }
  }
}
