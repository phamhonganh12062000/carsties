using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carsties.IdentityService.Provider.Pages.Lockout
{
  [AllowAnonymous]
  public class Index : PageModel
  {
    public void OnGet()
    {
    }
  }
}
