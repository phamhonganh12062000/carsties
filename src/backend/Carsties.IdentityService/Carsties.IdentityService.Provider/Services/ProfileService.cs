using System.Security.Claims;
using Carsties.IdentityService.Provider.Models;
using Duende.IdentityServer.Models;
using Duende.IdentityServer.Services;
using IdentityModel;
using Microsoft.AspNetCore.Identity;

namespace Carsties.IdentityService.Provider.Services
{
  public class ProfileService : IProfileService
  {
    private readonly UserManager<ApplicationUser> _userManager;

    public ProfileService(UserManager<ApplicationUser> userManager)
    {
      _userManager = userManager;
    }

    public async Task GetProfileDataAsync(ProfileDataRequestContext context)
    {
      var user = await _userManager.GetUserAsync(context.Subject);

      var existingClaims = await _userManager.GetClaimsAsync(user);

      // Send back username to token
      var claims = new List<Claim>
      {
        new Claim("username", user.UserName),
      };

      context.IssuedClaims.AddRange(claims);

      // Send back name to token
      context.IssuedClaims.Add(existingClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name));
    }

    public Task IsActiveAsync(IsActiveContext context)
    {
      return Task.CompletedTask;
    }
  }
}
