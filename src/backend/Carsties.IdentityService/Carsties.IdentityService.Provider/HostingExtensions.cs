using Carsties.IdentityService.Provider.Data;
using Carsties.IdentityService.Provider.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Carsties.IdentityService.Provider.Services;
using Carsties.EmailService;
using Carsties.Shared.Common.Logging;

namespace Carsties.IdentityService.Provider;

internal static class HostingExtensions
{
  public static WebApplication ConfigureServices(this WebApplicationBuilder builder)
  {
    builder.Services.AddRazorPages();

    builder.Services.AddDbContext<ApplicationDbContext>(options =>
        options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"),
        providerOptions => providerOptions.EnableRetryOnFailure()));

    builder.Services.AddIdentity<ApplicationUser, IdentityRole>((options) =>
    {
      options.User.RequireUniqueEmail = true;
      options.SignIn.RequireConfirmedEmail = true;
      options.Tokens.EmailConfirmationTokenProvider = "emailconfirmation";
      options.Lockout.AllowedForNewUsers = true;
      options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(1);
      options.Lockout.MaxFailedAccessAttempts = 5;
    })
        .AddEntityFrameworkStores<ApplicationDbContext>()
        .AddDefaultTokenProviders()
        .AddTokenProvider<EmailConfirmationTokenProvider<ApplicationUser>>("emailconfirmation");

    builder.Services
        .AddIdentityServer(options =>
        {
          options.Events.RaiseErrorEvents = true;
          options.Events.RaiseInformationEvents = true;
          options.Events.RaiseFailureEvents = true;
          options.Events.RaiseSuccessEvents = true;

          if (builder.Environment.IsEnvironment("Docker"))
          {
            options.IssuerUri = "identity-svc";
          }

          // see https://docs.duendesoftware.com/identityserver/v6/fundamentals/resources/
          // options.EmitStaticAudienceClaim = true;
        })
        .AddInMemoryIdentityResources(Config.IdentityResources)
        .AddInMemoryApiScopes(Config.ApiScopes)
        // .AddInMemoryApiResources(Config.Apis)
        .AddInMemoryClients(Config.Clients)
        .AddAspNetIdentity<ApplicationUser>()
        .AddProfileService<ProfileService>();

    builder.Services.AddScoped<IEmailSenderFactory, EmailSenderFactory>()
      .Configure<EmailConfiguration>(builder.Configuration.GetSection("EmailConfiguration"));

    builder.Services.Configure<DataProtectionTokenProviderOptions>((opts) =>
    {
      opts.TokenLifespan = TimeSpan.FromHours(2);
    });

    builder.Services.Configure<EmailConfirmationTokenProviderOptions>((opts) =>
    {
      opts.TokenLifespan = TimeSpan.FromDays(3);
    });

    builder.Services.ConfigureApplicationCookie((opts) =>
    {
      opts.Cookie.SameSite = SameSiteMode.Lax;
    });

    builder.Services.AddSingleton<ILoggerManager, LoggerManager>();

    builder.Services.AddAuthentication();

    return builder.Build();
  }

  public static WebApplication ConfigurePipeline(this WebApplication app)
  {
    app.UseSerilogRequestLogging();

    if (app.Environment.IsDevelopment())
    {
      app.UseDeveloperExceptionPage();
    }

    app.UseStaticFiles();
    app.UseRouting();
    app.UseIdentityServer();
    app.UseAuthorization();

    app.MapRazorPages()
        .RequireAuthorization();

    return app;
  }
}
