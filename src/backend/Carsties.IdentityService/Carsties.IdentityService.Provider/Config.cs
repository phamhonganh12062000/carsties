﻿using Duende.IdentityServer;
using Duende.IdentityServer.Models;

namespace Carsties.IdentityService.Provider;

public static class Config
{
  public static IEnumerable<IdentityResource> IdentityResources =>
      [
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
      ];

  public static IEnumerable<ApiScope> ApiScopes =>
      [
            new ApiScope("auctionApp", "Auction app full access"),
      ];

  public static IEnumerable<Client> Clients =>
      [
            new Client
            {
                ClientId = "postman",
                ClientName = "Postman",
                AllowedScopes = { IdentityServerConstants.StandardScopes.OpenId, IdentityServerConstants.StandardScopes.Profile, "auctionApp" },
                RedirectUris = { "https://www.getpostman.com/oauth2/callback" },
                ClientSecrets = new[] { new Secret("NotASecret".Sha256()) },
                AllowedGrantTypes = { GrantType.ResourceOwnerPassword },
            },
            new Client
            {
                ClientId = "nextApp",
                ClientName = "nextApp",
                ClientSecrets = { new Secret("secret".Sha256()) },
                AllowedGrantTypes = GrantTypes.CodeAndClientCredentials,
                RequirePkce = false,
                RedirectUris = { "http://localhost:3000/api/auth/callback/id-server" },
                AllowOfflineAccess = true,
                AllowedScopes = { IdentityServerConstants.StandardScopes.OpenId, IdentityServerConstants.StandardScopes.Profile, "auctionApp" },
                AccessTokenLifetime = 3600 * 24 * 30,
                AlwaysIncludeUserClaimsInIdToken = true,
            },
      ];
}
