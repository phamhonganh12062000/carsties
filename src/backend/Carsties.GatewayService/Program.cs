using Carsties.Shared.Common.Exceptions;
using Carsties.Shared.Common.Logging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Serilog;
using Serilog.Events;

var builder = WebApplication.CreateBuilder(args);
var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{environment}.json", optional: true)
        .AddEnvironmentVariables();

Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        .Enrich.FromLogContext()
        .CreateBootstrapLogger();

builder.Services.AddSingleton<ILoggerManager, LoggerManager>();
builder.Host.UseSerilog((context, services, configuration) => configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services)
        .Enrich.FromLogContext());
builder.Services.AddReverseProxy().LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"));
builder.Services.AddAuthentication((opts) =>
{
  opts.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
})
  .AddJwtBearer(options =>
  {
    options.Authority = builder.Configuration["IdentityServiceUrl"];
    options.RequireHttpsMetadata = false;
    options.TokenValidationParameters.ValidateAudience = false;
    options.TokenValidationParameters.NameClaimType = "username";
  });

builder.Services.AddCors(options =>
{
  options.AddPolicy("customPolicy", b =>
  {
    b.AllowAnyHeader()
          .AllowAnyMethod().AllowCredentials().WithOrigins(builder.Configuration["ClientApp"]);
  });
});

var app = builder.Build();
app.UseSerilogRequestLogging();
var logger = app.Services.GetRequiredService<ILoggerManager>();
app.ConfigureGlobalExceptionHandler(logger);
app.UseCors();
app.MapReverseProxy();
app.UseAuthentication();
app.UseAuthorization();

app.Run();
