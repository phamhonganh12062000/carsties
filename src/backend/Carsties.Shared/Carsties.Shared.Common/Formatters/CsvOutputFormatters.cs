using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;

namespace Carsties.Shared.Common.Formatters
{
  public class CsvOutputFormatters<TDto> : TextOutputFormatter
    where TDto : class
  {
    public CsvOutputFormatters()
    {
      SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/csv"));
      SupportedEncodings.Add(Encoding.UTF8);
      SupportedEncodings.Add(Encoding.Unicode);

    }

    protected override bool CanWriteType(Type? type)
    {
      if (typeof(TDto).IsAssignableFrom(type) || typeof(IEnumerable<TDto>).IsAssignableFrom(type))
      {
        return base.CanWriteType(type);
      }

      return false;
    }

    private static void FormatCsv(StringBuilder buffer, TDto item)
    {
      var properties = typeof(TDto).GetProperties(BindingFlags.Public | BindingFlags.Instance);

      var line = string.Join(",", properties.Select(p => $"\"{p.GetValue(item, null)}\""));

      buffer.Append(line);
    }

    public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
    {
      var response = context.HttpContext.Response;

      var buffer = new StringBuilder();

      if (context.Object is IEnumerable<TDto> enumerable)
      {
        foreach (var item in enumerable)
        {
          FormatCsv(buffer, item);
        }
      }
      else
      {
        FormatCsv(buffer, (TDto)context.Object);
      }

      await response.WriteAsync(buffer.ToString());
    }
  }
}
