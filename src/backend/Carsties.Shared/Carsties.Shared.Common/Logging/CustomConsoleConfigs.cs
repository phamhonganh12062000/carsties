using Serilog.Sinks.SystemConsole.Themes;

namespace Carsties.Shared.Common.Logging
{
  public static class CustomTemplate
  {
    public static AnsiConsoleTheme CustomTheme { get; } = new(
        new Dictionary<ConsoleThemeStyle, string>
        {
          [ConsoleThemeStyle.Text] = "\x1b[38;5;025m",       // Unknown
          [ConsoleThemeStyle.SecondaryText] = "\x1b[38;5;008m",  // Black
          [ConsoleThemeStyle.TertiaryText] = "\x1b[38;5;008m",   // Black
          [ConsoleThemeStyle.Invalid] = "\x1b[33;1m",           // Yellow
          [ConsoleThemeStyle.Null] = "\x1b[38;5;008m",          // Black
          [ConsoleThemeStyle.Name] = "\x1b[38;5;008m",          // Black
          [ConsoleThemeStyle.String] = "\x1b[38;5;086m",        // Cyan
          [ConsoleThemeStyle.Number] = "\x1b[38;5;151m",        // Light Red
          [ConsoleThemeStyle.Boolean] = "\x1b[38;5;215m",       // Light Magenta
          [ConsoleThemeStyle.Scalar] = "\x1b[38;5;077m",        // Dark Cyan
          [ConsoleThemeStyle.LevelVerbose] = "\u001b[30m",
          [ConsoleThemeStyle.LevelDebug] = "\u001b[44;1m\u001b[37;1m",
          [ConsoleThemeStyle.LevelInformation] = "\u001b[42;1m\u001b[37;1m",
          [ConsoleThemeStyle.LevelWarning] = "\u001b[43;1m\u001b[37;1m",
          [ConsoleThemeStyle.LevelError] = "\u001b[41;1m\u001b[37;1m",
          [ConsoleThemeStyle.LevelFatal] = "\u001b[46;1m\u001b[37;1m",
        });
  }

}
