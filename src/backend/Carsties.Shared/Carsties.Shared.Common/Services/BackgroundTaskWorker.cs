using Carsties.Shared.Common.Logging;
using Microsoft.Extensions.Hosting;

namespace Carsties.Shared.Common.Services
{
  public class BackgroundTaskWorker : BackgroundService
  {
    private readonly IBackgroundTaskQueue _taskQueue;
    private readonly ILoggerManager _logger;

    public BackgroundTaskWorker(IBackgroundTaskQueue taskQueue, ILoggerManager logger)
    {
      _taskQueue = taskQueue;
      _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
      _logger.LogInfo("Background task worker is starting.");

      while (!stoppingToken.IsCancellationRequested)
      {
        var workItem = await _taskQueue.DequeueAsync(stoppingToken);
        try
        {
          await workItem(stoppingToken);
          await Task.Delay(5000, stoppingToken);
        }
        catch (Exception ex)
        {
          _logger.LogError(ex, "Error occurred executing background task.");
        }
      }

      _logger.LogInfo("Background task worker is stopping.");
    }
  }
}
