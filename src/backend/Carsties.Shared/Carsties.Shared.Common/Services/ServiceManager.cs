namespace Carsties.Shared.Common.Services
{
  public sealed class ServiceManager<TService> : IServiceManager<TService>
    where TService : class
  {
    private readonly Lazy<TService> _service;

    public ServiceManager(Func<TService> serviceFactory)
    {
      _service = new Lazy<TService>(serviceFactory);
    }

    public TService Service => _service.Value;
  }
}
