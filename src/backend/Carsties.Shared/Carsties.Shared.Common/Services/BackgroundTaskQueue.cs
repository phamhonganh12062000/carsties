using System.Collections.Concurrent;

namespace Carsties.Shared.Common.Services
{
  public class BackgroundTaskQueue : IBackgroundTaskQueue
  {
    private readonly BlockingCollection<Func<CancellationToken, ValueTask>> _workItems = new();
    private readonly SemaphoreSlim _signal = new(0);

    public async Task<Func<CancellationToken, ValueTask>> DequeueAsync(CancellationToken cancellationToken)
    {
      await _signal.WaitAsync(cancellationToken);
      _workItems.TryTake(out var workItem);

      return workItem!;
    }

    public void Enqueue(Func<CancellationToken, ValueTask> workItem)
    {
      if (workItem == null)
      {
        throw new ArgumentNullException(nameof(workItem));
      }

      _workItems.Add(workItem);
      _signal.Release();
    }
  }
}
