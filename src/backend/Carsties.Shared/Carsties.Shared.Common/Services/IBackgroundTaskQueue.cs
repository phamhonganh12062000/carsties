namespace Carsties.Shared.Common.Services
{
  public interface IBackgroundTaskQueue
  {
    void Enqueue(Func<CancellationToken, ValueTask> workItem);
    Task<Func<CancellationToken, ValueTask>> DequeueAsync(CancellationToken cancellationToken);
  }
}
