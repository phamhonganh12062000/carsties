﻿using Carsties.Shared.Common.Caching;
using Microsoft.Extensions.Caching.Distributed;

namespace Carsties.Shared.Common;

public interface ICacheWrapper
{
  Task SetAsync<T>(TypedCacheKey key, T value);

  Task SetAsync<T>(TypedCacheKey key, T value, DistributedCacheEntryOptions options);

  bool TryGetValue<T>(TypedCacheKey key, out T? value);

  Task<T?> GetOrSetAsync<T>(TypedCacheKey key, Func<Task<T>> task, DistributedCacheEntryOptions? options = null);

  Task RemoveAsync(TypedCacheKey key);
}
