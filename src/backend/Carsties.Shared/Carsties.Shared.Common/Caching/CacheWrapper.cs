﻿using Carsties.Shared.Common.Caching;
using Microsoft.Extensions.Caching.Distributed;

namespace Carsties.Shared.Common;

public class CacheWrapper : ICacheWrapper
{
  private readonly IDistributedCache _cache;

  public CacheWrapper(IDistributedCache cache)
  {
    _cache = cache;
  }

  public async Task<T?> GetOrSetAsync<T>(TypedCacheKey key, Func<Task<T>> task, DistributedCacheEntryOptions? options = null)
  {
    return await Caching.DistributedCacheExtensions.GetOrSetAsync(_cache, key, task, options);
  }

  public async Task RemoveAsync(TypedCacheKey key)
  {
    await Caching.DistributedCacheExtensions.RemoveAsync(_cache, key);
  }

  public async Task SetAsync<T>(TypedCacheKey key, T value)
  {
    await Caching.DistributedCacheExtensions.SetAsync(_cache, key, value);
  }

  public async Task SetAsync<T>(TypedCacheKey key, T value, DistributedCacheEntryOptions options)
  {
    await Caching.DistributedCacheExtensions.SetAsync(_cache, key, value, options);
  }

  public bool TryGetValue<T>(TypedCacheKey key, out T? value)
  {
    return Caching.DistributedCacheExtensions.TryGetValue(_cache, key, out value);
  }
}
