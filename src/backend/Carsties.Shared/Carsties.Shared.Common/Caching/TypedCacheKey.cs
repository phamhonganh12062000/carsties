namespace Carsties.Shared.Common.Caching
{
  /// <summary>
  /// Typed cache keys instead of strings.
  /// </summary>
  public class TypedCacheKey
  {
    private readonly string _key;

    public TypedCacheKey(string prefix, string key, int version)
    {
      Prefix = prefix;
      Key = key;
      Version = version;
      _key = $"{Prefix}-{Key}-{Version}";
    }

    public string Prefix { get; init; }

    public string Key { get; init; }

    public int Version { get; init; }

    public static implicit operator string(TypedCacheKey generator) => generator._key;

    public static TypedCacheKey GetAllAuctionsCacheKey()
    {
      return new TypedCacheKey("GetAllAuction", "1", 1);
    }

    public static TypedCacheKey GetSingleAuctionCacheKey(Guid id)
    {
      return new TypedCacheKey("GetSingleAuction", id.ToString(), 1);
    }
  }
}
