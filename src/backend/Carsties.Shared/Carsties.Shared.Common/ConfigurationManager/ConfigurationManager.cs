﻿using Microsoft.Extensions.Configuration;

namespace Carsties.Shared.Common;

public class ConfigurationManager
{
  private static ConfigurationManager _instance;
  private readonly IConfiguration _configuration;

  private ConfigurationManager(IConfiguration configuration)
  {
    _configuration = configuration;
  }

  public static void Initialize(IConfiguration configuration)
  {
    _instance = new ConfigurationManager(configuration);
  }

  public static string GetValue(string key)
  {
    return _instance._configuration[key];
  }
}
