namespace Carsties.Shared.Common.ResponseHelpers
{
  /// <summary>
  /// Non-generic response wrapper
  /// </summary>
  public class Result
  {
    public bool IsSuccess { get; set; }

    public string ErrorMessage { get; set; }

    public static Result Success()
    {
      return new Result { IsSuccess = true };
    }

    public static Result Failure(string errorMessage)
    {
      return new Result { IsSuccess = false, ErrorMessage = errorMessage };
    }
  }

  /// <summary>
  /// Generic response wrapper
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class Result<T>
  {
    public bool IsSuccess { get; set; }

    public T Value { get; set; }

    public string ErrorMessage { get; set; }

    public static Result<T> Success(T value)
    {
      return new Result<T> { IsSuccess = true, Value = value };
    }

    public static Result<T> Failure(string errorMessage)
    {
      return new Result<T> { IsSuccess = false, ErrorMessage = errorMessage };
    }
  }
}
