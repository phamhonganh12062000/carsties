using NCrontab;

namespace Carsties.Shared.Common.Jobs
{
  public sealed record CronRegistryEntry(Type Type, CrontabSchedule CrontabSchedule);
}
