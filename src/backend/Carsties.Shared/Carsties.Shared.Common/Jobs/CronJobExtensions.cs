using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NCrontab;

namespace Carsties.Shared.Common.Jobs
{
  public static class CronJobExtensions
  {
    public static IServiceCollection AddCronJob<T>(this IServiceCollection services, string cronExpression)
        where T : class, ICronJob
    {
      var cron = CrontabSchedule.TryParse(cronExpression)
        ?? throw new ArgumentException("Invalid cron expression", nameof(cronExpression));

      var entry = new CronRegistryEntry(typeof(T), cron);

      services.AddHostedService<CronScheduler>();
      services.TryAddSingleton<T>(); // Check for duplicated registration
      services.AddSingleton(entry);

      return services;
    }
  }
}
