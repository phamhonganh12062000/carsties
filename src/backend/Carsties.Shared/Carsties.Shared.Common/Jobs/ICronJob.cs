namespace Carsties.Shared.Common.Jobs
{
  public interface ICronJob
  {
    Task Run(CancellationToken token = default);
  }
}
