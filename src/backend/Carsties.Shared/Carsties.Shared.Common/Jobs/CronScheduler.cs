using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Carsties.Shared.Common.Jobs
{
  public sealed class CronScheduler : BackgroundService
  {
    private readonly IServiceProvider _serviceProvider;
    private readonly IReadOnlyCollection<CronRegistryEntry> _cronJobs;

    public CronScheduler(IServiceProvider serviceProvider, IEnumerable<CronRegistryEntry> cronJobs)
    {
      _serviceProvider = serviceProvider;
      _cronJobs = cronJobs.ToList();
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
      // Every 30 seconds pass (a tick completed), the code inside the loop runs
      // As cron only works with 1-minute-level precision
      // Everything less than 1 minute has to be handled outside of cron
      using var tickTimer = new PeriodicTimer(TimeSpan.FromSeconds(30));

      var runMap = new Dictionary<DateTime, List<Type>>();
      while (await tickTimer.WaitForNextTickAsync(stoppingToken))
      {
        var now = UtcNowMinutePrecision();

        RunActiveJobs(runMap, now, stoppingToken);

        //Get the next run for the upcoming tick
        runMap = GetJobRuns();
      }
    }

    private void RunActiveJobs(IReadOnlyDictionary<DateTime, List<Type>> runMap, DateTime now, CancellationToken cancellationToken)
    {
      if (!runMap.TryGetValue(now, out var currentRuns))
      {
        return;
      }

      foreach (var run in currentRuns)
      {
        var job = (ICronJob)_serviceProvider.GetRequiredService(run);

        // No explicit await for jobs as it might interfere with other job runs (wait for jobs before it to complete)
        // Doing this allows jobs to run in parallel or concurrently
        job.Run(cancellationToken);
      }
    }

    private Dictionary<DateTime, List<Type>> GetJobRuns()
    {
      var runMap = new Dictionary<DateTime, List<Type>>();

      foreach (var cronJob in _cronJobs)
      {
        var utcNow = DateTime.UtcNow;
        var runDates = cronJob.CrontabSchedule.GetNextOccurrences(utcNow, utcNow.AddMinutes(1));

        if (runDates is not null)
        {
          AddJobRuns(runMap, runDates, cronJob);
        }
      }

      return runMap;
    }

    private static void AddJobRuns(IDictionary<DateTime, List<Type>> runMap, IEnumerable<DateTime> runDates, CronRegistryEntry cronJob)
    {
      foreach (var runDate in runDates)
      {
        if (runMap.TryGetValue(runDate, out var value))
        {
          value.Add(cronJob.Type);
        }
        else
        {
          // If current runDate not in the hash map
          runMap[runDate] = new List<Type> { cronJob.Type };
        }
      }
    }

    private static DateTime UtcNowMinutePrecision()
    {
      var now = DateTime.UtcNow;
      return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0);
    }
  }
}
