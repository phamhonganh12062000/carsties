using Carsties.Shared.Common.ResponseHelpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace Carsties.Shared.Common.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class BaseController : ControllerBase
  {
    [NonAction]
    protected IActionResult HandleResult<T>(Result<T> result, string routeName = null, object routeValues = null)
    {
      if (result == null)
      {
        return NotFound();
      }

      if (result.IsSuccess)
      {
        if (result.Value == null)
        {
          return NotFound();
        }
        else
        {
          if (string.IsNullOrEmpty(routeName))
          {
            return Ok(result.Value);
          }
          else
          {
            return CreatedAtRoute(routeName, routeValues, result.Value);
          }
        }
      }

      return BadRequest(result.ErrorMessage);
    }

    [NonAction]
    protected IActionResult HandleResult(Result result)
    {
      if (result is null)
      {
        return NotFound();
      }

      if (result.IsSuccess)
      {
        return Ok();
      }

      return BadRequest(result.ErrorMessage);
    }
  }
}
