namespace Carsties.Shared.Common.Exceptions
{
    public sealed class ValidationIdentityException : Exception
    {
        public IReadOnlyDictionary<string, string[]> Errors { get; }

        public ValidationIdentityException(string fieldName, string errorMessage)
            : base("One or more validation errors occurred.")
        {
            Errors = new Dictionary<string, string[]>
            {
                { fieldName, new[] { errorMessage } },
            };
        }
    }
}
