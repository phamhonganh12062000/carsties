using System.Net;
using System.Text.Json;
using Carsties.Shared.Common.ErrorModels;
using Carsties.Shared.Common.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace Carsties.Shared.Common.Exceptions
{
  public static class GlobalExceptionHandler
  {
    public static void ConfigureGlobalExceptionHandler(this WebApplication app, ILoggerManager logger)
    {
      app.UseExceptionHandler(appError =>
      {
        appError.Run(async (httpContext) => await HandleExceptions(httpContext, logger));
      });
    }

    private static async Task HandleExceptions(HttpContext httpContext, ILoggerManager logger)
    {
      SetDefaultResponseSettings(httpContext);

      // Represents a feature containing the error of the original request to be examined by an exception handler.
      var contextFeature = httpContext.Features.Get<IExceptionHandlerFeature>();

      if (contextFeature != null)
      {
        logger.LogError($"Something went wrong: {contextFeature.Error}");
        HandleErrorStatusCode(httpContext, contextFeature);

        await WriteErrorResponse(httpContext, contextFeature);
      }
    }

    private static void SetDefaultResponseSettings(HttpContext httpContext)
    {
      httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

      httpContext.Response.ContentType = "application/json";
    }

    private static void HandleErrorStatusCode(HttpContext httpContext, IExceptionHandlerFeature exceptionHandlerFeature)
    {
      httpContext.Response.StatusCode = exceptionHandlerFeature.Error switch
      {
        NotFoundException => StatusCodes.Status404NotFound,
        BadRequestException => StatusCodes.Status400BadRequest,
        ValidationAppException => StatusCodes.Status422UnprocessableEntity,
        ValidationIdentityException => StatusCodes.Status400BadRequest,
        ForbiddenException => StatusCodes.Status403Forbidden,
        UnauthorizedAccessException => StatusCodes.Status401Unauthorized,
        _ => StatusCodes.Status500InternalServerError,
      };
    }

    private static async Task WriteErrorResponse(HttpContext httpContext, IExceptionHandlerFeature contextFeature)
    {
      var options = new JsonSerializerOptions
      {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
      };

      switch (contextFeature.Error)
      {
        case ValidationAppException validationAppException:
          await WriteValidationErrorResponse(httpContext, validationAppException, options);
          break;
        case ValidationIdentityException validationIdentityException:
          await WriteValidationErrorResponse(httpContext, validationIdentityException, options);
          break;
        default:
          await WriteDefaultErrorResponse(httpContext, contextFeature, options);
          break;
      }
    }

    private static async Task WriteValidationErrorResponse(HttpContext httpContext, Exception validationException, JsonSerializerOptions options)
    {
      await httpContext.Response.WriteAsync(JsonSerializer.Serialize(
          new
          {
            ((dynamic)validationException).Errors,
          },
          options));
    }

    private static async Task WriteDefaultErrorResponse(HttpContext httpContext, IExceptionHandlerFeature contextFeature, JsonSerializerOptions options)
    {
      await httpContext.Response.WriteAsync(JsonSerializer.Serialize(
          new ErrorDetails()
          {
            StatusCode = httpContext.Response.StatusCode,
            Message = contextFeature.Error.Message,
            StackTrace = contextFeature.Error.StackTrace,
          }, options));
    }
  }
}
