namespace Carsties.Shared.Common.Exceptions
{
  public class ForbiddenException : Exception
  {
    public ForbiddenException(string message)
      : base(message)
    {
    }
  }
}
