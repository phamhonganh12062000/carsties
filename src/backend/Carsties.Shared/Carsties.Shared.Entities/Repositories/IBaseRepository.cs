using System.Linq.Expressions;

namespace Carsties.Shared.Entities.Repositories
{
  public interface IBaseRepository<T>
  {
    Task<IEnumerable<T>> GetAllAsync(bool trackChanges = default);

    IQueryable<T> Find(Expression<Func<T, bool>> condition, bool trackChanges = default, params Expression<Func<T, object>>[] includedProperties);

    Task<T> FindFirstOrDefaultAsync(Expression<Func<T, bool>> condition, bool trackChanges = default, params Expression<Func<T, object>>[] includedProperties);

    Task AddAsync(T entity);

    void Update(T entity);

    void Remove(T entity);

    Task<int> SaveChangesAsync();
  }
}
