using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace Carsties.Shared.Entities.Repositories
{
  public abstract class BaseRepository<T> : IBaseRepository<T>
    where T : class
  {
    protected readonly DbContext _dbContext;

    public BaseRepository(DbContext dbContext)
    {
      _dbContext = dbContext;
    }

    public async Task AddAsync(T entity)
    {
      await _dbContext.Set<T>().AddAsync(entity);
    }

    public IQueryable<T> Find(Expression<Func<T, bool>> condition, bool trackChanges = default, params Expression<Func<T, object>>[] includedProperties)
    {
      IQueryable<T> query = _dbContext.Set<T>().Where(condition);

      if (includedProperties != null)
      {
        foreach (var includedProperty in includedProperties)
        {
          query = query.Include(includedProperty);
        }
      }

      if (!trackChanges)
      {
        return query.AsNoTracking();
      }

      return query;
    }

    public async Task<T> FindFirstOrDefaultAsync(Expression<Func<T, bool>> condition, bool trackChanges = default, params Expression<Func<T, object>>[] includedProperties)
    {
      IQueryable<T> query = _dbContext.Set<T>().Where(condition);

      if (includedProperties != null)
      {
        foreach (var includedProperty in includedProperties)
        {
          query = query.Include(includedProperty);
        }
      }

      if (!trackChanges)
      {
        return await query
            .AsNoTracking()
            .FirstOrDefaultAsync(condition);
      }
      else
      {
        return await query
            .FirstOrDefaultAsync(condition);
      }
    }

    public async Task<IEnumerable<T>> GetAllAsync(bool trackChanges = default)
    {
      IQueryable<T> query = _dbContext.Set<T>();

      if (!trackChanges)
      {
        return query.AsNoTracking();
      }

      return query;
    }

    public void Remove(T entity)
    {
      _dbContext.Set<T>().Remove(entity);
    }

    public async Task<int> SaveChangesAsync()
    {
      return await _dbContext.SaveChangesAsync();
    }

    public void Update(T entity)
    {
      _dbContext.Set<T>().Update(entity);
    }
  }
}
