namespace Carsties.Shared.Entities.Events
{
  public class AuctionDeleted
  {
    public string Id { get; set; }
  }
}
