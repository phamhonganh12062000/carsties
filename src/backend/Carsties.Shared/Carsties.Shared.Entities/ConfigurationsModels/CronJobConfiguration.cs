namespace Carsties.Shared.Entities.ConfigurationModels
{
  public class CronJobConfiguration
  {
    public string ServiceUrl { get; set; }

    public string TriggerTime { get; set; }
  }
}
