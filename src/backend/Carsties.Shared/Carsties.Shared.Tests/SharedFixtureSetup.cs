using AutoFixture;

namespace Carsties.Shared.Tests;

public class SharedFixtureSetup
{
  public Fixture Fixture { get; private set; }

  public SharedFixtureSetup()
  {
    Fixture = new Fixture();

    // Create a copy of enumeration
    var behaviorsToRemove = Fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList();
    foreach (var behavior in behaviorsToRemove)
    {
      Fixture.Behaviors.Remove(behavior);
    }

    Fixture.Behaviors.Add(new OmitOnRecursionBehavior());
  }
}
