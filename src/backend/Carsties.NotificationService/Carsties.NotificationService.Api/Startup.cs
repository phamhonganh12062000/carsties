using Carsties.NotificationService.Api.Consumers;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.ConfigurationModels;
using MassTransit;
using Serilog;

namespace Carsties.NotificationService.Api
{
  public static class Startup
  {
    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddSingleton<ILoggerManager, LoggerManager>();
      services.AddSignalR();
      services.AddMassTransit((opt) =>
      {
        var rabbitMqConfiguration = configuration.GetSection(nameof(RabbitMqConfiguration)).Get<RabbitMqConfiguration>()
        ?? throw new InvalidOperationException($"{nameof(RabbitMqConfiguration)} settings not found in configuration.");

        opt.AddConsumersFromNamespaceContaining<AuctionCreatedConsumer>();

        opt.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter("notifications", false));

        opt.UsingRabbitMq((context, config) =>
        {
          config.Host(rabbitMqConfiguration.Host, "/", (host) =>
          {
            host.Username(rabbitMqConfiguration.Username ?? "guest");
            host.Password(rabbitMqConfiguration.Password ?? "guest");
          });

          config.ConfigureEndpoints(context);
        });
      });
    }

    public static void ConfigureHost(this IHostBuilder hostBuilder)
    {
      hostBuilder.UseSerilog((context, services, configuration) => configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services)
        .Enrich.FromLogContext());
    }
  }
}
