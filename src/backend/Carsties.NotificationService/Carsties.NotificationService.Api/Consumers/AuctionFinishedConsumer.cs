using Carsties.NotificationService.Api.Hubs;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Entities;

namespace Carsties.NotificationService.Api.Consumers
{
  public class AunctionFinishedConsumer : IConsumer<AuctionFinished>
  {
    private readonly IHubContext<NotificationHub> _hubContext;
    private readonly ILoggerManager _logger;

    public AunctionFinishedConsumer(IHubContext<NotificationHub> hubContext, ILoggerManager logger)
    {
      _hubContext = hubContext;
      _logger = logger;
    }

    public async Task Consume(ConsumeContext<AuctionFinished> context)
    {
      _logger.LogInfo("--> aunction created message received");

      await _hubContext.Clients.All.SendAsync(nameof(AuctionFinished), context.Message);
    }
  }
}
