using Carsties.NotificationService.Api.Hubs;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;
using Microsoft.AspNetCore.SignalR;

namespace Carsties.NotificationService.Api.Consumers;
public class BidPlacedConsumer : IConsumer<BidPlaced>
{
  private readonly IHubContext<NotificationHub> _hubContext;
  private readonly ILoggerManager _logger;

  public BidPlacedConsumer(IHubContext<NotificationHub> hubContext, ILoggerManager logger)
  {
    _hubContext = hubContext;
    _logger = logger;
  }

  public async Task Consume(ConsumeContext<BidPlaced> context)
  {
    _logger.LogInfo("--> bid placed message received");

    await _hubContext.Clients.All.SendAsync("BidPlaced", context.Message);
  }
}
