using Carsties.NotificationService.Api.Hubs;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;
using Microsoft.AspNetCore.SignalR;

namespace Carsties.NotificationService.Api.Consumers
{
  public class AuctionCreatedConsumer : IConsumer<AuctionCreated>
  {
    private readonly IHubContext<NotificationHub> _hubContext;
    private readonly ILoggerManager _logger;

    public AuctionCreatedConsumer(IHubContext<NotificationHub> hubContext, ILoggerManager logger)
    {
      _hubContext = hubContext;
      _logger = logger;
    }

    public async Task Consume(ConsumeContext<AuctionCreated> context)
    {
      _logger.LogInfo("--> aunction created message received");

      await _hubContext.Clients.All.SendAsync(nameof(AuctionCreated), context.Message);
    }
  }
}
