using Carsties.NotificationService.Api;
using Carsties.NotificationService.Api.Hubs;
using Carsties.Shared.Common.Exceptions;
using Carsties.Shared.Common.Logging;
using Serilog;
using Serilog.Events;

var builder = WebApplication.CreateBuilder(args);

var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{environment}.json", optional: true)
        .AddEnvironmentVariables();

Carsties.Shared.Common.ConfigurationManager.Initialize(builder.Configuration);

Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        .Enrich.FromLogContext()
        .CreateBootstrapLogger();

builder.Services.ConfigureServices(builder.Configuration);
builder.Host.ConfigureHost();
var app = builder.Build();
app.UseSerilogRequestLogging();
var logger = app.Services.GetRequiredService<ILoggerManager>();
app.ConfigureGlobalExceptionHandler(logger);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.MapHub<NotificationHub>("/notifications");
app.Run();
