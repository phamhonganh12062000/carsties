using AutoFixture;
using AutoMapper;
using Carsties.AuctionService.BusinessLogic.Repositories;
using Carsties.AuctionService.BusinessLogic.Services;
using Carsties.AuctionService.Entities.Dtos;
using Carsties.AuctionService.Entities.Models;
using Carsties.Shared.Common;
using Carsties.Shared.Common.Caching;
using Carsties.Shared.Common.Exceptions;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Common.ResponseHelpers;
using Carsties.Shared.Tests;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Moq;

namespace Carsties.AuctionService.Tests.ServiceTests;

public class AuctionServices_Test
{
  private readonly Mock<IAuctionRepository> _auctionRepositoryMock;
  private readonly Mock<IMapper> _mapperMock;
  private readonly Mock<ILoggerManager> _loggerMock;
  private readonly Mock<IPublishEndpoint> _publishEndpointMock;
  private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock;
  private readonly Mock<ICacheWrapper> _cacheWrapperMock;
  private readonly SharedFixtureSetup _sharedFixtureSetup;

  private readonly AuctionServices _auctionServices;

  public AuctionServices_Test()
  {
    _sharedFixtureSetup = new SharedFixtureSetup();
    _auctionRepositoryMock = new Mock<IAuctionRepository>();
    _mapperMock = new Mock<IMapper>();
    _loggerMock = new Mock<ILoggerManager>();
    _publishEndpointMock = new Mock<IPublishEndpoint>();
    _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
    _cacheWrapperMock = new Mock<ICacheWrapper>();
    _auctionServices = new AuctionServices(_auctionRepositoryMock.Object, _mapperMock.Object, _loggerMock.Object, _publishEndpointMock.Object, _httpContextAccessorMock.Object, _cacheWrapperMock.Object);
  }

  [Fact]
  public async Task GetAllAuctions_WithNoParams_Returns10Auctions()
  {
    // ARRANGE
    var auctions = _sharedFixtureSetup.Fixture.CreateMany<Auction>(10).ToList();

    var auctionsDto = _sharedFixtureSetup.Fixture.CreateMany<AuctionDto>(10).ToList();

    _auctionRepositoryMock.Setup(r => r.GetAllAuctionsAsync()).ReturnsAsync(auctions);

    _cacheWrapperMock.Setup(c => c.GetOrSetAsync(
            It.IsAny<TypedCacheKey>(),
            It.IsAny<Func<Task<Result<IEnumerable<AuctionDto>>>>>(),
            It.IsAny<DistributedCacheEntryOptions>()))
        .ReturnsAsync(Result<IEnumerable<AuctionDto>>.Success(auctionsDto));

    _mapperMock.Setup(m => m.Map<IEnumerable<AuctionDto>>(auctions)).Returns(auctionsDto);

    // ACT
    var result = await _auctionServices.GetAllAuctions(null);

    // ASSERT
    Assert.Equal(10, result.Value.Count());
    Assert.IsType<Result<IEnumerable<AuctionDto>>>(result);
  }

  // [Fact]
  // public async Task GetAllAuctions_WithDate_ReturnsLatestAuctions()
  // {
  //   // ARRANGE
  //   var date = DateTime.UtcNow.AddDays(-1).ToString();

  //   var auctions = _sharedFixtureSetup.Fixture.CreateMany<Auction>(10).ToList();

  //   // Expression<Func<Auction, bool>> currentDateExpression = auction => auction.UpdatedAt.CompareTo(DateTime.Parse(date).ToUniversalTime()) > 0;

  //   var auctionsMock = auctions.BuildMock();

  //   var auctionsDto = _sharedFixtureSetup.Fixture.CreateMany<AuctionDto>(10).ToList();

  //   _auctionRepositoryMock.Setup(repo => repo.Find(It.IsAny<Expression<Func<Auction, bool>>>(), It.IsAny<bool>(), null))
  //       .Returns(auctionsMock);

  //   _mapperMock.Setup(m => m.Map<IEnumerable<AuctionDto>>(auctions)).Returns(auctionsDto);

  //   // ACT
  //   var result = await _auctionServices.GetAllAuctions(date);

  //   // ASSERT
  //   Assert.Equal(10, result.Value.Count());
  //   Assert.IsType<Result<IEnumerable<AuctionDto>>>(result);
  // }

  [Fact]
  public async Task GetAuctionById_WithValidGuid_ReturnsAuction()
  {
    // ARRANGE
    var auction = _sharedFixtureSetup.Fixture.Create<Auction>();

    var auctionDto = _sharedFixtureSetup.Fixture.Create<AuctionDto>();

    _auctionRepositoryMock.Setup(r => r.GetAuctionByIdAsync(It.IsAny<Guid>())).ReturnsAsync(auction);

    _mapperMock.Setup(m => m.Map<AuctionDto>(auction)).Returns(auctionDto);

    _cacheWrapperMock.Setup(c => c.GetOrSetAsync(
            It.IsAny<TypedCacheKey>(),
            It.IsAny<Func<Task<Result<AuctionDto>>>>(),
            It.IsAny<DistributedCacheEntryOptions>()))
        .ReturnsAsync(Result<AuctionDto>.Success(auctionDto));

    // ACT
    var result = await _auctionServices.GetAuctionById(auction.Id);

    // ASSERT
    Assert.Equal(auctionDto.Make, result.Value.Make);
    Assert.IsType<Result<AuctionDto>>(result);
  }

  [Fact]
  public async Task GetAuctionById_WithInvalidGuid_ReturnsNotFound()
  {
    // ARRANGE
    var invalidId = It.IsAny<Guid>();
    var notFoundException = new NotFoundException($"Auction with the id: {invalidId} was not found.");
    _auctionRepositoryMock.Setup(r => r.GetAuctionByIdAsync(invalidId)).ReturnsAsync(value: null);

    Moq.Language.Flow.IReturnsResult<ICacheWrapper> returnsResult = _cacheWrapperMock.Setup(c => c.GetOrSetAsync(
            It.IsAny<TypedCacheKey>(),
            It.IsAny<Func<Task<Result<AuctionDto>>>>(),
            It.IsAny<DistributedCacheEntryOptions>()))
        .ThrowsAsync(notFoundException);

    // ACT
    var exception = await Assert.ThrowsAsync<NotFoundException>(() => _auctionServices.GetAuctionById(invalidId));

    // ASSERT
    Assert.Equal($"Auction with the id: {invalidId} was not found.", exception.Message);
  }

  [Fact]
  public async Task CreateAuction_FailedSave_Returns400BadRequest()
  {
    // ARRANGE
    var auctionDto = _sharedFixtureSetup.Fixture.Create<CreateAuctionDto>();
    var auction = _sharedFixtureSetup.Fixture.Create<Auction>();
    _auctionRepositoryMock.Setup(r => r.AddAsync(It.IsAny<Auction>()));
    _auctionRepositoryMock.Setup(r => r.SaveChangesAsync()).ReturnsAsync(0);
    _mapperMock.Setup(m => m.Map<Auction>(auctionDto)).Returns(auction);
    _httpContextAccessorMock.Setup(r => r.HttpContext.User.Identity.Name).Returns("Seller Name");

    // ACT
    var result = await _auctionServices.CreateAuction(auctionDto);

    // ASSERT
    Assert.IsType<Result<AuctionDto>>(result);
    Assert.Equal(false, result.IsSuccess);
  }
}
