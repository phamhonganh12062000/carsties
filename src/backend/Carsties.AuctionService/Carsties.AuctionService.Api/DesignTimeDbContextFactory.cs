using Carsties.AuctionService.Entities.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Carsties.AuctionService.Api
{
  public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AuctionDbContext>
  {
    private string _environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

    public AuctionDbContext CreateDbContext(string[] args)
    {
      IConfigurationRoot configuration = new ConfigurationBuilder()
          .AddUserSecrets<Program>()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json", optional: false)
          .AddJsonFile($"appsettings.{_environment}.json", optional: true)
          .AddEnvironmentVariables()
          .Build();

      var builder = new DbContextOptionsBuilder<AuctionDbContext>();

      var connectionString = configuration.GetConnectionString("DefaultConnection");

      builder.UseNpgsql(connectionString, x => x.MigrationsAssembly(typeof(AuctionDbContext).Assembly.FullName)
          .EnableRetryOnFailure());

      return new AuctionDbContext(builder.Options);
    }
  }
}
