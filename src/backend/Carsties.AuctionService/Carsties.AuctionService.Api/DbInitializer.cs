using Carsties.AuctionService.Entities.DataAccess;
using Carsties.Shared.Common.Logging;
using Microsoft.EntityFrameworkCore;

namespace Carsties.AuctionService.Api
{
  public class DbInitializer
  {
    public static async Task SeedDataAsync(WebApplication app, ILoggerManager logger)
    {
      using var scope = app.Services.CreateScope();

      try
      {
        var dataContext = scope.ServiceProvider.GetRequiredService<AuctionDbContext>();
        await dataContext.Database.MigrateAsync();
        // DbInitializer.Initialize(userManager);
      }
      catch (Exception ex)
      {
        logger.LogError(ex, "An error has occurred during migration.");
        throw;
      }
    }
  }
}
