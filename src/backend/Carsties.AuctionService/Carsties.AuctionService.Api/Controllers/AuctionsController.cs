using Carsties.AuctionService.BusinessLogic.Services;
using Carsties.AuctionService.Entities.Dtos;
using Carsties.Shared.Common.ActionFilters;
using Carsties.Shared.Common.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Carsties.AuctionService.Api.Controllers
{
  public class AuctionsController : BaseController
  {
    private readonly IAuctionServices _auctionServices;

    public AuctionsController(IAuctionServices auctionServices)
    {
      _auctionServices = auctionServices;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllAuctions(string date)
    {
      return HandleResult(await _auctionServices.GetAllAuctions(date));
    }

    [HttpGet("{id:guid}", Name = "AuctionById")]
    public async Task<IActionResult> GetAuctionById(Guid id)
    {
      return HandleResult(await _auctionServices.GetAuctionById(id));
    }

    [Authorize]
    [HttpPost]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    public async Task<IActionResult> CreateAuction([FromBody] CreateAuctionDto createAuctionDto)
    {
      var result = await _auctionServices.CreateAuction(createAuctionDto);

      return HandleResult(result, "AuctionById", new { id = result.Value.Id });
    }

    [Authorize]
    [HttpPut("{id}")]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    public async Task<IActionResult> UpdateAuction(Guid id, [FromBody] UpdateAuctionDto updateAuctionDto)
    {
      return HandleResult(await _auctionServices.UpdateAuction(id, updateAuctionDto));
    }

    [Authorize]
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteAuction(Guid id)
    {
      return HandleResult(await _auctionServices.DeleteAuction(id));
    }
  }
}
