FROM mcr.microsoft.com/dotnet/sdk:8.0 as build
WORKDIR /app
EXPOSE 80

# Copy all .csproj files and restore as distinct layers
# Use the same COPY command for every dockerfile in the project to take advantage of docker caching
COPY Carsties.sln Carsties.sln

# Shared projects
COPY src/backend/Carsties.Shared/Carsties.Shared.Common/Carsties.Shared.Common.csproj src/backend/Carsties.Shared/Carsties.Shared.Common/Carsties.Shared.Common.csproj
COPY src/backend/Carsties.Shared/Carsties.Shared.Entities/Carsties.Shared.Entities.csproj src/backend/Carsties.Shared/Carsties.Shared.Entities/Carsties.Shared.Entities.csproj
COPY src/backend/Carsties.Shared/Carsties.Shared.Tests/Carsties.Shared.Tests.csproj src/backend/Carsties.Shared/Carsties.Shared.Tests/Carsties.Shared.Tests.csproj

# Auction service
COPY src/backend/Carsties.AuctionService/Carsties.AuctionService.Entities/Carsties.AuctionService.Entities.csproj src/backend/Carsties.AuctionService/Carsties.AuctionService.Entities/Carsties.AuctionService.Entities.csproj
COPY src/backend/Carsties.AuctionService/Carsties.AuctionService.BusinessLogic/Carsties.AuctionService.BusinessLogic.csproj src/backend/Carsties.AuctionService/Carsties.AuctionService.BusinessLogic/Carsties.AuctionService.BusinessLogic.csproj
COPY src/backend/Carsties.AuctionService/Carsties.AuctionService.Api/Carsties.AuctionService.Api.csproj src/backend/Carsties.AuctionService/Carsties.AuctionService.Api/Carsties.AuctionService.Api.csproj
COPY src/backend/Carsties.AuctionService/Carsties.AuctionService.Tests/Carsties.AuctionService.Tests.csproj src/backend/Carsties.AuctionService/Carsties.AuctionService.Tests/Carsties.AuctionService.Tests.csproj

# Search service
COPY src/backend/Carsties.SearchService/Carsties.SearchService.Entities/Carsties.SearchService.Entities.csproj src/backend/Carsties.SearchService/Carsties.SearchService.Entities/Carsties.SearchService.Entities.csproj
COPY src/backend/Carsties.SearchService/Carsties.SearchService.BusinessLogic/Carsties.SearchService.BusinessLogic.csproj src/backend/Carsties.SearchService/Carsties.SearchService.BusinessLogic/Carsties.SearchService.BusinessLogic.csproj
COPY src/backend/Carsties.SearchService/Carsties.SearchService.Api/Carsties.SearchService.Api.csproj src/backend/Carsties.SearchService/Carsties.SearchService.Api/Carsties.SearchService.Api.csproj
COPY src/backend/Carsties.SearchService/Carsties.SearchService.Tests/Carsties.SearchService.Tests.csproj src/backend/Carsties.SearchService/Carsties.SearchService.Tests/Carsties.SearchService.Tests.csproj

# Identity service
COPY src/backend/Carsties.EmailService/Carsties.EmailService.csproj src/backend/Carsties.EmailService/Carsties.EmailService.csproj
COPY src/backend/Carsties.IdentityService/Carsties.IdentityService.Provider/Carsties.IdentityService.Provider.csproj src/backend/Carsties.IdentityService/Carsties.IdentityService.Provider/Carsties.IdentityService.Provider.csproj

# Gateway service
COPY src/backend/Carsties.GatewayService/Carsties.GatewayService.csproj src/backend/Carsties.GatewayService/Carsties.GatewayService.csproj

# Bidding service
COPY src/backend/Carsties.BiddingService/Carsties.BiddingService.Api/Carsties.BiddingService.Api.csproj src/backend/Carsties.BiddingService/Carsties.BiddingService.Api/Carsties.BiddingService.Api.csproj
COPY src/backend/Carsties.BiddingService/Carsties.BiddingService.Tests/Carsties.BiddingService.Tests.csproj src/backend/Carsties.BiddingService/Carsties.BiddingService.Tests/Carsties.BiddingService.Tests.csproj

# Notification service
COPY src/backend/Carsties.NotificationService/Carsties.NotificationService.Api/Carsties.NotificationService.Api.csproj src/backend/Carsties.NotificationService/Carsties.NotificationService.Api/Carsties.NotificationService.Api.csproj
COPY src/backend/Carsties.NotificationService/Carsties.NotificationService.Tests/Carsties.NotificationService.Tests.csproj src/backend/Carsties.NotificationService/Carsties.NotificationService.Tests/Carsties.NotificationService.Tests.csproj

RUN dotnet restore Carsties.sln

COPY src/backend/Carsties.AuctionService/Carsties.AuctionService.Api src/backend/Carsties.AuctionService/Carsties.AuctionService.Api
#  Dependencies
COPY src/backend/Carsties.AuctionService/Carsties.AuctionService.BusinessLogic src/backend/Carsties.AuctionService/Carsties.AuctionService.BusinessLogic
COPY src/backend/Carsties.AuctionService/Carsties.AuctionService.Entities src/backend/Carsties.AuctionService/Carsties.AuctionService.Entities
COPY src/backend/Carsties.Shared/Carsties.Shared.Common src/backend/Carsties.Shared/Carsties.Shared.Common
COPY src/backend/Carsties.Shared/Carsties.Shared.Entities src/backend/Carsties.Shared/Carsties.Shared.Entities
WORKDIR /app/src/backend/Carsties.AuctionService/Carsties.AuctionService.Api
RUN dotnet publish -c Release -o /app/src/backend/out

FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /app
COPY --from=build /app/src/backend/out .
ENTRYPOINT [ "dotnet", "Carsties.AuctionService.Api.dll" ]
