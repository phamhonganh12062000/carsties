using Carsties.AuctionService.BusinessLogic.Consumers;
using Carsties.AuctionService.Entities.DataAccess;
using Carsties.Shared.Entities.ConfigurationModels;
using MassTransit;
using Carsties.AuctionService.Entities.Dtos;
using Carsties.Shared.Common.Formatters;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Carsties.AuctionService.Api
{
  public static class Startup
  {
    public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddDbContext<AuctionDbContext>(options
              => options.UseNpgsql(
                  configuration.GetConnectionString("DefaultConnection"),
                  providerOptions => providerOptions.EnableRetryOnFailure()));
    }

    public static void ConfigureSerilog(this IHostBuilder hostBuilder)
    {
      hostBuilder.UseSerilog((context, services, configuration) => configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services)
        .Enrich.FromLogContext());
    }

    public static void ConfigureHttpContextAccessor(this IServiceCollection services)
    {
      services.AddHttpContextAccessor();
    }

    public static void ConfigureJWT(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddAuthentication((opts) =>
      {
        opts.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        // opts.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        // opts.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
      })

        // Register cookie-based authentication
        // .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme)

        // Register JWT-based authentication
        .AddJwtBearer((opts) =>
        {
          opts.Authority = configuration["IdentityServiceUrl"];
          opts.RequireHttpsMetadata = false;
          opts.TokenValidationParameters.ValidateAudience = false;
          opts.TokenValidationParameters.NameClaimType = "username";
        })

        // OpenID connect set as default for the authentication actions
        // .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, (opts) => {
        //   opts.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        //   opts.Authority = configuration["IdentityServiceUrl"];
        //   opts.ClientId = "auctionserviceclient";
        //   opts.ResponseType = OpenIdConnectResponseType.Code; // Expect the code to be returned from /authorization  endpoint
        //   opts.SaveTokens = true;
        //   opts.ClientSecret = "AuctionServiceClientSecret";
        //   opts.UsePkce = false;
        // })
        ;
    }

    public static void ConfigureMassTransit(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddMassTransit((opt) =>
      {
        var rabbitMqConfiguration = configuration.GetSection(nameof(RabbitMqConfiguration)).Get<RabbitMqConfiguration>()
        ?? throw new InvalidOperationException($"{nameof(RabbitMqConfiguration)} settings not found in configuration.");

        // Use an outbox to store messages when RabbitMQ is down
        opt.AddEntityFrameworkOutbox<AuctionDbContext>(x =>
        {
          x.QueryDelay = TimeSpan.FromSeconds(10); // Hardcoding
          x.UsePostgres();
          x.UseBusOutbox();
        });

        opt.AddConsumersFromNamespaceContaining<AuctionCreatedFaultConsumer>();

        opt.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter("auctions", false));

        opt.UsingRabbitMq((context, config) =>
        {
          config.Host(rabbitMqConfiguration.Host, "/", (host) =>
          {
            host.Username(rabbitMqConfiguration.Username ?? "guest");
            host.Password(rabbitMqConfiguration.Password ?? "guest");
          });

          config.ConfigureEndpoints(context);
        });
      });
    }

    public static void ConfigureRedisServer(this IServiceCollection services)
    {
      services.AddStackExchangeRedisCache(options =>
      {
        options.Configuration = Shared.Common.ConfigurationManager.GetValue("RedisURL");
        options.ConfigurationOptions = new StackExchange.Redis.ConfigurationOptions()
        {
          AbortOnConnectFail = true,
          EndPoints = { options.Configuration },
        };
      });
    }

    public static IMvcBuilder AddCustomFormatters(this IMvcBuilder builder)
      => builder.AddMvcOptions((config) => config.OutputFormatters.Add(new CsvOutputFormatters<AuctionDto>()));

    // public static void ConfigureHttpClient(this IServiceCollection services)
    // {
    //   services.AddHttpClient("AuctionServiceClient", (client) => {
    //     client.BaseAddress = new Uri("http://localhost:7001");
    //     client.DefaultRequestHeaders.Clear();
    //     client.DefaultRequestHeaders.Add(HeaderNames.Accept, "application/json");
    //   });
    // }
  }
}
