namespace Carsties.AuctionService.Api
{
  using Carsties.AuctionService.BusinessLogic.Repositories;
  using Carsties.AuctionService.BusinessLogic.Services;
  using Carsties.Shared.Common;
  using Carsties.Shared.Common.ActionFilters;
  using Carsties.Shared.Common.Logging;

  public static class DependencyInjections
  {
    public static void AddLoggingService(this IServiceCollection services)
    {
      services.AddSingleton<ILoggerManager, LoggerManager>();
    }

    public static void AddServices(this IServiceCollection services)
    {
      services.AddScoped<IAuctionRepository, AuctionRepository>();
      services.AddScoped<IAuctionServices, AuctionServices>();
      services.AddSingleton<ICacheWrapper, CacheWrapper>();
    }

    public static void AddValidationFilters(this IServiceCollection services)
    {
      services.AddScoped<ValidationFilterAttribute>();
    }
  }
}
