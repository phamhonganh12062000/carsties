using Carsties.AuctionService.Api;
using Carsties.AuctionService.BusinessLogic.Services;
using Carsties.Shared.Common.Exceptions;
using Carsties.Shared.Common.Logging;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Serilog.Events;

var builder = WebApplication.CreateBuilder(args);
var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{environment}.json", optional: true)
        .AddEnvironmentVariables();

Carsties.Shared.Common.ConfigurationManager.Initialize(builder.Configuration);

Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        .Enrich.FromLogContext()
        .CreateBootstrapLogger();

builder.Services.AddLoggingService();
builder.Services.AddServices();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAuthorization();
builder.Services.AddControllers((config) =>
{
  config.RespectBrowserAcceptHeader = true;
  config.ReturnHttpNotAcceptable = true;
}).AddXmlDataContractSerializerFormatters()
  .AddCustomFormatters();
builder.Services.AddAutoMapper(typeof(Carsties.AuctionService.BusinessLogic.AssemblyReference).Assembly);
builder.Services.AddValidationFilters();

builder.Services.Configure<ApiBehaviorOptions>(options =>
{
  options.SuppressModelStateInvalidFilter = true;
});

builder.Services.ConfigureMassTransit(builder.Configuration);
builder.Host.ConfigureSerilog();
builder.Services.ConfigureSqlContext(builder.Configuration);
builder.Services.ConfigureJWT(builder.Configuration);
builder.Services.ConfigureHttpContextAccessor();
builder.Services.ConfigureRedisServer();
builder.Services.AddGrpc();

var app = builder.Build();

app.UseSerilogRequestLogging();
var logger = app.Services.GetRequiredService<ILoggerManager>();
app.ConfigureGlobalExceptionHandler(logger);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGrpcService<GrpcAuctionService>();

await DbInitializer.SeedDataAsync(app, logger);

app.Run();
