using System.ComponentModel.DataAnnotations;

namespace Carsties.AuctionService.Entities.Dtos
{
  public record CreateAuctionDto
  {
    [Required]
    public string Make { get; init; }

    [Required]
    public string Model { get; init; }

    [Required]
    public int Year { get; init; }

    [Required]
    public string Color { get; init; }

    [Required]
    public int Mileage { get; init; }

    [Required]
    public string ImageUrl { get; init; }

    [Required]
    public int ReservePrice { get; init; }

    [Required]
    public DateTime AuctionEnd { get; init; }
  }
}
