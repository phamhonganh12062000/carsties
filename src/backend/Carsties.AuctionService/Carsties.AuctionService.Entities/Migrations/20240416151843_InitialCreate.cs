﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Carsties.AuctionService.Entities.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Auctions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ReservePrice = table.Column<int>(type: "integer", nullable: false),
                    Seller = table.Column<string>(type: "text", nullable: false),
                    Winner = table.Column<string>(type: "text", nullable: true),
                    SoldAmount = table.Column<int>(type: "integer", nullable: true),
                    CurrentHighBid = table.Column<int>(type: "integer", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    AuctionEnd = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auctions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Make = table.Column<string>(type: "text", nullable: false),
                    Model = table.Column<string>(type: "text", nullable: false),
                    Year = table.Column<int>(type: "integer", nullable: false),
                    Color = table.Column<string>(type: "text", nullable: false),
                    Mileage = table.Column<int>(type: "integer", nullable: false),
                    ImageUrl = table.Column<string>(type: "text", nullable: false),
                    AuctionId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_Auctions_AuctionId",
                        column: x => x.AuctionId,
                        principalTable: "Auctions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Auctions",
                columns: new[] { "Id", "AuctionEnd", "CreatedAt", "CurrentHighBid", "ReservePrice", "Seller", "SoldAmount", "Status", "UpdatedAt", "Winner" },
                values: new object[,]
                {
                    { new Guid("155225c1-4448-4066-9886-6786536e05ea"), new DateTime(2024, 4, 6, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6026), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6024), null, 50000, "tom", null, 2, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6025), null },
                    { new Guid("3659ac24-29dd-407a-81f5-ecfe6f924b9b"), new DateTime(2024, 6, 3, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6038), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6037), null, 20000, "bob", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6037), null },
                    { new Guid("40490065-dac7-46b6-acc4-df507e0d6570"), new DateTime(2024, 5, 6, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6036), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6035), null, 20000, "tom", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6035), null },
                    { new Guid("466e4744-4dc5-4987-aae0-b621acfc5e39"), new DateTime(2024, 5, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6028), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6026), null, 20000, "alice", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6027), null },
                    { new Guid("47111973-d176-4feb-848d-0ea22641c31a"), new DateTime(2024, 4, 29, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6032), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6031), null, 150000, "alice", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6031), null },
                    { new Guid("6a5011a1-fe1f-47df-9a32-b5346b289391"), new DateTime(2024, 5, 5, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6034), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6033), null, 0, "bob", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6033), null },
                    { new Guid("afbee524-5972-4075-8800-7d1f9d7b0a0c"), new DateTime(2024, 4, 26, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6011), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6007), null, 20000, "bob", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6009), null },
                    { new Guid("bbab4d5a-8565-48b1-9450-5ac2a5c4a654"), new DateTime(2024, 4, 20, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6023), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6022), null, 0, "bob", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6022), null },
                    { new Guid("c8c3ec17-01bf-49db-82aa-1ef80b833a9f"), new DateTime(2024, 6, 15, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6021), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6019), null, 90000, "alice", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6020), null },
                    { new Guid("dc1e4071-d19d-459b-b848-b5c3cd3d151f"), new DateTime(2024, 5, 31, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6030), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6028), null, 20000, "bob", null, 0, new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6029), null }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "AuctionId", "Color", "ImageUrl", "Make", "Mileage", "Model", "Year" },
                values: new object[,]
                {
                    { new Guid("1b799f24-f60b-4b46-b100-75b8329d1801"), new Guid("6a5011a1-fe1f-47df-9a32-b5346b289391"), "White", "https://cdn.pixabay.com/photo/2019/12/26/20/50/audi-r8-4721217_960_720.jpg", "Audi", 10050, "R8", 2021 },
                    { new Guid("23724124-b1c4-4141-9c50-d9b91e407027"), new Guid("bbab4d5a-8565-48b1-9450-5ac2a5c4a654"), "Black", "https://cdn.pixabay.com/photo/2012/11/02/13/02/car-63930_960_720.jpg", "Ford", 65125, "Mustang", 2023 },
                    { new Guid("66fde739-4a55-4487-81d7-c326a1476f54"), new Guid("c8c3ec17-01bf-49db-82aa-1ef80b833a9f"), "Black", "https://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278_960_720.jpg", "Bugatti", 15035, "Veyron", 2018 },
                    { new Guid("67b0981d-3577-425d-a725-fa37fa4cb421"), new Guid("afbee524-5972-4075-8800-7d1f9d7b0a0c"), "White", "https://cdn.pixabay.com/photo/2016/05/06/16/32/car-1376190_960_720.jpg", "Ford", 50000, "GT", 2020 },
                    { new Guid("74063d4a-b7cb-4319-9fe1-f95840e2588d"), new Guid("155225c1-4448-4066-9886-6786536e05ea"), "Silver", "https://cdn.pixabay.com/photo/2016/04/17/22/10/mercedes-benz-1335674_960_720.png", "Mercedes", 15001, "SLK", 2020 },
                    { new Guid("ca721bc5-e94f-4b12-a359-87011943668c"), new Guid("3659ac24-29dd-407a-81f5-ecfe6f924b9b"), "Rust", "https://cdn.pixabay.com/photo/2017/08/02/19/47/vintage-2573090_960_720.jpg", "Ford", 150150, "Model T", 1938 },
                    { new Guid("cac0f849-c044-4625-a971-5791c033eb90"), new Guid("47111973-d176-4feb-848d-0ea22641c31a"), "Red", "https://cdn.pixabay.com/photo/2017/11/08/14/39/ferrari-f430-2930661_960_720.jpg", "Ferrari", 5000, "F-430", 2022 },
                    { new Guid("cebe39be-27cf-45e2-9d96-91f016993809"), new Guid("40490065-dac7-46b6-acc4-df507e0d6570"), "Black", "https://cdn.pixabay.com/photo/2016/09/01/15/06/audi-1636320_960_720.jpg", "Audi", 25400, "TT", 2020 },
                    { new Guid("e03a1e9b-5f92-4ab7-9d34-464494f14576"), new Guid("466e4744-4dc5-4987-aae0-b621acfc5e39"), "White", "https://cdn.pixabay.com/photo/2017/08/31/05/47/bmw-2699538_960_720.jpg", "BMW", 90000, "X1", 2017 },
                    { new Guid("e72fbcfa-7660-449a-90a4-fa8ac3078baa"), new Guid("dc1e4071-d19d-459b-b848-b5c3cd3d151f"), "Red", "https://cdn.pixabay.com/photo/2017/11/09/01/49/ferrari-458-spider-2932191_960_720.jpg", "Ferrari", 50000, "Spider", 2015 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_AuctionId",
                table: "Items",
                column: "AuctionId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Auctions");
        }
    }
}
