﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Carsties.AuctionService.Entities.Migrations
{
    /// <inheritdoc />
    public partial class AddOutbox : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("1b799f24-f60b-4b46-b100-75b8329d1801"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("23724124-b1c4-4141-9c50-d9b91e407027"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("66fde739-4a55-4487-81d7-c326a1476f54"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("67b0981d-3577-425d-a725-fa37fa4cb421"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("74063d4a-b7cb-4319-9fe1-f95840e2588d"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("ca721bc5-e94f-4b12-a359-87011943668c"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("cac0f849-c044-4625-a971-5791c033eb90"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("cebe39be-27cf-45e2-9d96-91f016993809"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("e03a1e9b-5f92-4ab7-9d34-464494f14576"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("e72fbcfa-7660-449a-90a4-fa8ac3078baa"));

            migrationBuilder.CreateTable(
                name: "InboxState",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MessageId = table.Column<Guid>(type: "uuid", nullable: false),
                    ConsumerId = table.Column<Guid>(type: "uuid", nullable: false),
                    LockId = table.Column<Guid>(type: "uuid", nullable: false),
                    RowVersion = table.Column<byte[]>(type: "bytea", rowVersion: true, nullable: true),
                    Received = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    ReceiveCount = table.Column<int>(type: "integer", nullable: false),
                    ExpirationTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    Consumed = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    Delivered = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    LastSequenceNumber = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InboxState", x => x.Id);
                    table.UniqueConstraint("AK_InboxState_MessageId_ConsumerId", x => new { x.MessageId, x.ConsumerId });
                });

            migrationBuilder.CreateTable(
                name: "OutboxMessage",
                columns: table => new
                {
                    SequenceNumber = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EnqueueTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    SentTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Headers = table.Column<string>(type: "text", nullable: true),
                    Properties = table.Column<string>(type: "text", nullable: true),
                    InboxMessageId = table.Column<Guid>(type: "uuid", nullable: true),
                    InboxConsumerId = table.Column<Guid>(type: "uuid", nullable: true),
                    OutboxId = table.Column<Guid>(type: "uuid", nullable: true),
                    MessageId = table.Column<Guid>(type: "uuid", nullable: false),
                    ContentType = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    MessageType = table.Column<string>(type: "text", nullable: false),
                    Body = table.Column<string>(type: "text", nullable: false),
                    ConversationId = table.Column<Guid>(type: "uuid", nullable: true),
                    CorrelationId = table.Column<Guid>(type: "uuid", nullable: true),
                    InitiatorId = table.Column<Guid>(type: "uuid", nullable: true),
                    RequestId = table.Column<Guid>(type: "uuid", nullable: true),
                    SourceAddress = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    DestinationAddress = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ResponseAddress = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    FaultAddress = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ExpirationTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutboxMessage", x => x.SequenceNumber);
                });

            migrationBuilder.CreateTable(
                name: "OutboxState",
                columns: table => new
                {
                    OutboxId = table.Column<Guid>(type: "uuid", nullable: false),
                    LockId = table.Column<Guid>(type: "uuid", nullable: false),
                    RowVersion = table.Column<byte[]>(type: "bytea", rowVersion: true, nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Delivered = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    LastSequenceNumber = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutboxState", x => x.OutboxId);
                });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("155225c1-4448-4066-9886-6786536e05ea"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 4, 20, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6537), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6536), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6536) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("3659ac24-29dd-407a-81f5-ecfe6f924b9b"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 6, 17, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6549), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6547), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6548) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("40490065-dac7-46b6-acc4-df507e0d6570"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 20, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6547), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6545), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6546) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("466e4744-4dc5-4987-aae0-b621acfc5e39"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6539), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6538), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6538) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("47111973-d176-4feb-848d-0ea22641c31a"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 13, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6543), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6542), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6542) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("6a5011a1-fe1f-47df-9a32-b5346b289391"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 19, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6545), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6544), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6544) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("afbee524-5972-4075-8800-7d1f9d7b0a0c"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 10, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6524), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6519), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6522) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("bbab4d5a-8565-48b1-9450-5ac2a5c4a654"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 4, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6535), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6534), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6534) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("c8c3ec17-01bf-49db-82aa-1ef80b833a9f"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 6, 29, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6532), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6531), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6531) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("dc1e4071-d19d-459b-b848-b5c3cd3d151f"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 6, 14, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6541), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6540), new DateTime(2024, 4, 30, 7, 40, 9, 595, DateTimeKind.Utc).AddTicks(6540) });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "AuctionId", "Color", "ImageUrl", "Make", "Mileage", "Model", "Year" },
                values: new object[,]
                {
                    { new Guid("0bc8f7b2-ef83-44ac-b410-156ba67c773b"), new Guid("afbee524-5972-4075-8800-7d1f9d7b0a0c"), "White", "https://cdn.pixabay.com/photo/2016/05/06/16/32/car-1376190_960_720.jpg", "Ford", 50000, "GT", 2020 },
                    { new Guid("17a03c50-fb72-4acb-b3b9-c29c5c1b8cac"), new Guid("6a5011a1-fe1f-47df-9a32-b5346b289391"), "White", "https://cdn.pixabay.com/photo/2019/12/26/20/50/audi-r8-4721217_960_720.jpg", "Audi", 10050, "R8", 2021 },
                    { new Guid("23b8a242-9c5a-4020-a962-68e9c50c3918"), new Guid("bbab4d5a-8565-48b1-9450-5ac2a5c4a654"), "Black", "https://cdn.pixabay.com/photo/2012/11/02/13/02/car-63930_960_720.jpg", "Ford", 65125, "Mustang", 2023 },
                    { new Guid("274993f3-d18d-4096-9c80-f21c6470e189"), new Guid("dc1e4071-d19d-459b-b848-b5c3cd3d151f"), "Red", "https://cdn.pixabay.com/photo/2017/11/09/01/49/ferrari-458-spider-2932191_960_720.jpg", "Ferrari", 50000, "Spider", 2015 },
                    { new Guid("5cc54fad-5e30-4bb8-91fa-6ca3d0634d50"), new Guid("40490065-dac7-46b6-acc4-df507e0d6570"), "Black", "https://cdn.pixabay.com/photo/2016/09/01/15/06/audi-1636320_960_720.jpg", "Audi", 25400, "TT", 2020 },
                    { new Guid("6736eb68-feb0-4759-99c0-07c3a25aa71b"), new Guid("c8c3ec17-01bf-49db-82aa-1ef80b833a9f"), "Black", "https://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278_960_720.jpg", "Bugatti", 15035, "Veyron", 2018 },
                    { new Guid("82741e6e-949d-4e34-88e5-8bc4d80e8fdc"), new Guid("466e4744-4dc5-4987-aae0-b621acfc5e39"), "White", "https://cdn.pixabay.com/photo/2017/08/31/05/47/bmw-2699538_960_720.jpg", "BMW", 90000, "X1", 2017 },
                    { new Guid("8a407ba6-867f-43dc-bdb1-753d40947be7"), new Guid("155225c1-4448-4066-9886-6786536e05ea"), "Silver", "https://cdn.pixabay.com/photo/2016/04/17/22/10/mercedes-benz-1335674_960_720.png", "Mercedes", 15001, "SLK", 2020 },
                    { new Guid("a69b225a-52e1-4504-b100-308ab267c948"), new Guid("47111973-d176-4feb-848d-0ea22641c31a"), "Red", "https://cdn.pixabay.com/photo/2017/11/08/14/39/ferrari-f430-2930661_960_720.jpg", "Ferrari", 5000, "F-430", 2022 },
                    { new Guid("d3ea7431-0c72-42ca-a322-872009e13083"), new Guid("3659ac24-29dd-407a-81f5-ecfe6f924b9b"), "Rust", "https://cdn.pixabay.com/photo/2017/08/02/19/47/vintage-2573090_960_720.jpg", "Ford", 150150, "Model T", 1938 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_InboxState_Delivered",
                table: "InboxState",
                column: "Delivered");

            migrationBuilder.CreateIndex(
                name: "IX_OutboxMessage_EnqueueTime",
                table: "OutboxMessage",
                column: "EnqueueTime");

            migrationBuilder.CreateIndex(
                name: "IX_OutboxMessage_ExpirationTime",
                table: "OutboxMessage",
                column: "ExpirationTime");

            migrationBuilder.CreateIndex(
                name: "IX_OutboxMessage_InboxMessageId_InboxConsumerId_SequenceNumber",
                table: "OutboxMessage",
                columns: new[] { "InboxMessageId", "InboxConsumerId", "SequenceNumber" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OutboxMessage_OutboxId_SequenceNumber",
                table: "OutboxMessage",
                columns: new[] { "OutboxId", "SequenceNumber" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OutboxState_Created",
                table: "OutboxState",
                column: "Created");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InboxState");

            migrationBuilder.DropTable(
                name: "OutboxMessage");

            migrationBuilder.DropTable(
                name: "OutboxState");

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("0bc8f7b2-ef83-44ac-b410-156ba67c773b"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("17a03c50-fb72-4acb-b3b9-c29c5c1b8cac"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("23b8a242-9c5a-4020-a962-68e9c50c3918"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("274993f3-d18d-4096-9c80-f21c6470e189"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("5cc54fad-5e30-4bb8-91fa-6ca3d0634d50"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("6736eb68-feb0-4759-99c0-07c3a25aa71b"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("82741e6e-949d-4e34-88e5-8bc4d80e8fdc"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("8a407ba6-867f-43dc-bdb1-753d40947be7"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("a69b225a-52e1-4504-b100-308ab267c948"));

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "Id",
                keyValue: new Guid("d3ea7431-0c72-42ca-a322-872009e13083"));

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("155225c1-4448-4066-9886-6786536e05ea"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 4, 6, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6026), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6024), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6025) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("3659ac24-29dd-407a-81f5-ecfe6f924b9b"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 6, 3, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6038), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6037), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6037) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("40490065-dac7-46b6-acc4-df507e0d6570"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 6, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6036), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6035), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6035) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("466e4744-4dc5-4987-aae0-b621acfc5e39"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6028), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6026), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6027) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("47111973-d176-4feb-848d-0ea22641c31a"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 4, 29, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6032), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6031), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6031) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("6a5011a1-fe1f-47df-9a32-b5346b289391"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 5, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6034), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6033), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6033) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("afbee524-5972-4075-8800-7d1f9d7b0a0c"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 4, 26, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6011), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6007), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6009) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("bbab4d5a-8565-48b1-9450-5ac2a5c4a654"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 4, 20, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6023), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6022), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6022) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("c8c3ec17-01bf-49db-82aa-1ef80b833a9f"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 6, 15, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6021), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6019), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6020) });

            migrationBuilder.UpdateData(
                table: "Auctions",
                keyColumn: "Id",
                keyValue: new Guid("dc1e4071-d19d-459b-b848-b5c3cd3d151f"),
                columns: new[] { "AuctionEnd", "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2024, 5, 31, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6030), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6028), new DateTime(2024, 4, 16, 15, 18, 42, 709, DateTimeKind.Utc).AddTicks(6029) });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "AuctionId", "Color", "ImageUrl", "Make", "Mileage", "Model", "Year" },
                values: new object[,]
                {
                    { new Guid("1b799f24-f60b-4b46-b100-75b8329d1801"), new Guid("6a5011a1-fe1f-47df-9a32-b5346b289391"), "White", "https://cdn.pixabay.com/photo/2019/12/26/20/50/audi-r8-4721217_960_720.jpg", "Audi", 10050, "R8", 2021 },
                    { new Guid("23724124-b1c4-4141-9c50-d9b91e407027"), new Guid("bbab4d5a-8565-48b1-9450-5ac2a5c4a654"), "Black", "https://cdn.pixabay.com/photo/2012/11/02/13/02/car-63930_960_720.jpg", "Ford", 65125, "Mustang", 2023 },
                    { new Guid("66fde739-4a55-4487-81d7-c326a1476f54"), new Guid("c8c3ec17-01bf-49db-82aa-1ef80b833a9f"), "Black", "https://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278_960_720.jpg", "Bugatti", 15035, "Veyron", 2018 },
                    { new Guid("67b0981d-3577-425d-a725-fa37fa4cb421"), new Guid("afbee524-5972-4075-8800-7d1f9d7b0a0c"), "White", "https://cdn.pixabay.com/photo/2016/05/06/16/32/car-1376190_960_720.jpg", "Ford", 50000, "GT", 2020 },
                    { new Guid("74063d4a-b7cb-4319-9fe1-f95840e2588d"), new Guid("155225c1-4448-4066-9886-6786536e05ea"), "Silver", "https://cdn.pixabay.com/photo/2016/04/17/22/10/mercedes-benz-1335674_960_720.png", "Mercedes", 15001, "SLK", 2020 },
                    { new Guid("ca721bc5-e94f-4b12-a359-87011943668c"), new Guid("3659ac24-29dd-407a-81f5-ecfe6f924b9b"), "Rust", "https://cdn.pixabay.com/photo/2017/08/02/19/47/vintage-2573090_960_720.jpg", "Ford", 150150, "Model T", 1938 },
                    { new Guid("cac0f849-c044-4625-a971-5791c033eb90"), new Guid("47111973-d176-4feb-848d-0ea22641c31a"), "Red", "https://cdn.pixabay.com/photo/2017/11/08/14/39/ferrari-f430-2930661_960_720.jpg", "Ferrari", 5000, "F-430", 2022 },
                    { new Guid("cebe39be-27cf-45e2-9d96-91f016993809"), new Guid("40490065-dac7-46b6-acc4-df507e0d6570"), "Black", "https://cdn.pixabay.com/photo/2016/09/01/15/06/audi-1636320_960_720.jpg", "Audi", 25400, "TT", 2020 },
                    { new Guid("e03a1e9b-5f92-4ab7-9d34-464494f14576"), new Guid("466e4744-4dc5-4987-aae0-b621acfc5e39"), "White", "https://cdn.pixabay.com/photo/2017/08/31/05/47/bmw-2699538_960_720.jpg", "BMW", 90000, "X1", 2017 },
                    { new Guid("e72fbcfa-7660-449a-90a4-fa8ac3078baa"), new Guid("dc1e4071-d19d-459b-b848-b5c3cd3d151f"), "Red", "https://cdn.pixabay.com/photo/2017/11/09/01/49/ferrari-458-spider-2932191_960_720.jpg", "Ferrari", 50000, "Spider", 2015 }
                });
        }
    }
}
