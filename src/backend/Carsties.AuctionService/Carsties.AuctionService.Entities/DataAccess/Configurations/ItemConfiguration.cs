using Carsties.AuctionService.Entities.DataAccess.Static;
using Carsties.AuctionService.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Carsties.AuctionService.Entities.DataAccess.Configurations
{
  public class ItemConfiguration : IEntityTypeConfiguration<Item>
  {
    public void Configure(EntityTypeBuilder<Item> builder)
    {
      builder.HasData(
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Ford",
          Model = "GT",
          Color = "White",
          Mileage = 50000,
          Year = 2020,
          ImageUrl = "https://cdn.pixabay.com/photo/2016/05/06/16/32/car-1376190_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_1_FORD_GT,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Bugatti",
          Model = "Veyron",
          Color = "Black",
          Mileage = 15035,
          Year = 2018,
          ImageUrl = "https://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_2_BUGATTI_VEYRON,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Ford",
          Model = "Mustang",
          Color = "Black",
          Mileage = 65125,
          Year = 2023,
          ImageUrl = "https://cdn.pixabay.com/photo/2012/11/02/13/02/car-63930_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_3_FORD_MUSTANG,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Mercedes",
          Model = "SLK",
          Color = "Silver",
          Mileage = 15001,
          Year = 2020,
          ImageUrl = "https://cdn.pixabay.com/photo/2016/04/17/22/10/mercedes-benz-1335674_960_720.png",
          AuctionId = AuctionSeedData.AUCTION_4_MERCEDES_SLK,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "BMW",
          Model = "X1",
          Color = "White",
          Mileage = 90000,
          Year = 2017,
          ImageUrl = "https://cdn.pixabay.com/photo/2017/08/31/05/47/bmw-2699538_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_5_BMW_X1,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Ferrari",
          Model = "Spider",
          Color = "Red",
          Mileage = 50000,
          Year = 2015,
          ImageUrl = "https://cdn.pixabay.com/photo/2017/11/09/01/49/ferrari-458-spider-2932191_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_6_FERRARI_SPIDER,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Ferrari",
          Model = "F-430",
          Color = "Red",
          Mileage = 5000,
          Year = 2022,
          ImageUrl = "https://cdn.pixabay.com/photo/2017/11/08/14/39/ferrari-f430-2930661_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_7_FERRARI_F430,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Audi",
          Model = "R8",
          Color = "White",
          Mileage = 10050,
          Year = 2021,
          ImageUrl = "https://cdn.pixabay.com/photo/2019/12/26/20/50/audi-r8-4721217_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_8_AUDI_R8,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Audi",
          Model = "TT",
          Color = "Black",
          Mileage = 25400,
          Year = 2020,
          ImageUrl = "https://cdn.pixabay.com/photo/2016/09/01/15/06/audi-1636320_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_9_AUDI_TT,
        },
        new Item
        {
          Id = Guid.NewGuid(),
          Make = "Ford",
          Model = "Model T",
          Color = "Rust",
          Mileage = 150150,
          Year = 1938,
          ImageUrl = "https://cdn.pixabay.com/photo/2017/08/02/19/47/vintage-2573090_960_720.jpg",
          AuctionId = AuctionSeedData.AUCTION_10_FORD_MODEL_T,
        }
      );
    }
  }
}
