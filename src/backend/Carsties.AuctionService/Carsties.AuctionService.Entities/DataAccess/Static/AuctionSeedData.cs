namespace Carsties.AuctionService.Entities.DataAccess.Static
{
  public static class AuctionSeedData
  {
    public static readonly Guid AUCTION_1_FORD_GT = Guid.Parse("afbee524-5972-4075-8800-7d1f9d7b0a0c");
    public static readonly Guid AUCTION_2_BUGATTI_VEYRON = Guid.Parse("c8c3ec17-01bf-49db-82aa-1ef80b833a9f");
    public static readonly Guid AUCTION_3_FORD_MUSTANG = Guid.Parse("bbab4d5a-8565-48b1-9450-5ac2a5c4a654");
    public static readonly Guid AUCTION_4_MERCEDES_SLK = Guid.Parse("155225c1-4448-4066-9886-6786536e05ea");
    public static readonly Guid AUCTION_5_BMW_X1 = Guid.Parse("466e4744-4dc5-4987-aae0-b621acfc5e39");
    public static readonly Guid AUCTION_6_FERRARI_SPIDER = Guid.Parse("dc1e4071-d19d-459b-b848-b5c3cd3d151f");
    public static readonly Guid AUCTION_7_FERRARI_F430 = Guid.Parse("47111973-d176-4feb-848d-0ea22641c31a");
    public static readonly Guid AUCTION_8_AUDI_R8 = Guid.Parse("6a5011a1-fe1f-47df-9a32-b5346b289391");
    public static readonly Guid AUCTION_9_AUDI_TT= Guid.Parse("40490065-dac7-46b6-acc4-df507e0d6570");
    public static readonly Guid AUCTION_10_FORD_MODEL_T = Guid.Parse("3659ac24-29dd-407a-81f5-ecfe6f924b9b");

  }
}
