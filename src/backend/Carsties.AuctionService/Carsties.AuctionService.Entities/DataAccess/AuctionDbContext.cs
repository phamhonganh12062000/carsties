﻿using Carsties.AuctionService.Entities.DataAccess.Configurations;
using Carsties.AuctionService.Entities.Models;
using MassTransit;
using Microsoft.EntityFrameworkCore;

namespace Carsties.AuctionService.Entities.DataAccess;

public class AuctionDbContext : DbContext
{
  public AuctionDbContext(DbContextOptions<AuctionDbContext> options)
    : base(options)
  {
  }

  protected override void OnModelCreating(ModelBuilder modelBuilder)
  {
    base.OnModelCreating(modelBuilder);

    // Seed auction and item data
    modelBuilder.ApplyConfiguration(new ItemConfiguration());
    modelBuilder.ApplyConfiguration(new AuctionConfiguration());

    // Add outbox schema to store data when RabbitMQ is down
    modelBuilder.AddInboxStateEntity();
    modelBuilder.AddOutboxMessageEntity();
    modelBuilder.AddOutboxStateEntity();
  }

  public DbSet<Auction> Auctions { get; set; }
}
