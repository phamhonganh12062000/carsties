namespace Carsties.AuctionService.Entities.Enums;

public enum Status
{
  Live,
  Finished,
  ReserveNotMet
}
