using Carsties.AuctionService.Entities.DataAccess;
using Carsties.AuctionService.Entities.Models;
using Carsties.Shared.Entities.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Carsties.AuctionService.BusinessLogic.Repositories
{
  public class AuctionRepository : BaseRepository<Auction>, IAuctionRepository
  {
    private readonly AuctionDbContext _auctionDbContext;

    public AuctionRepository(AuctionDbContext auctionDbContext)
      : base(auctionDbContext)
    {
      _auctionDbContext = auctionDbContext;
    }

    public async Task CreateAuctionAsync(Auction auction)
    {
      await _auctionDbContext.AddAsync(auction);
    }

    public async Task<IEnumerable<Auction>> GetAllAuctionsAsync()
    {
      return await _auctionDbContext.Auctions
        .Include(a => a.Item)
        .OrderBy(a => a.Item.Make)
        .ToListAsync();
    }

    public async Task<Auction> GetAuctionByIdAsync(Guid id)
    {
      return await _auctionDbContext.Auctions
        .Include(a => a.Item)
        .FirstOrDefaultAsync(a => a.Id == id);
    }
  }
}
