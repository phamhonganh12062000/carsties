using Carsties.AuctionService.Entities.Models;
using Carsties.Shared.Entities.Repositories;

namespace Carsties.AuctionService.BusinessLogic.Repositories
{
  public interface IAuctionRepository : IBaseRepository<Auction>
  {
    Task<IEnumerable<Auction>> GetAllAuctionsAsync();

    Task<Auction> GetAuctionByIdAsync(Guid id);

    Task CreateAuctionAsync(Auction auction);
  }
}
