﻿using Carsties.AuctionService.BusinessLogic.Repositories;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;

namespace Carsties.AuctionService.BusinessLogic.Consumers;

public class BidPlacedConsumer : IConsumer<BidPlaced>
{
  private readonly ILoggerManager _logger;
  private readonly IAuctionRepository _auctionRepository;

  public BidPlacedConsumer(ILoggerManager logger, IAuctionRepository auctionRepository)
  {
    _logger = logger;
    _auctionRepository = auctionRepository;
  }

  public async Task Consume(ConsumeContext<BidPlaced> context)
  {
    _logger.LogInfo("--> Consuming bid placed");

    var auction = await _auctionRepository.GetAuctionByIdAsync(Guid.Parse(context.Message.AuctionId));

    if (auction.CurrentHighBid is null
      || (context.Message.BidStatus.Contains("Accepted") && context.Message.Amount > auction.CurrentHighBid))
    {
      auction.CurrentHighBid = context.Message.Amount;
      await _auctionRepository.SaveChangesAsync();
    }
  }
}
