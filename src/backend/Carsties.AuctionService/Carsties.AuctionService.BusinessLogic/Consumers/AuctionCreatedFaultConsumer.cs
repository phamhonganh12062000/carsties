using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;

namespace Carsties.AuctionService.BusinessLogic.Consumers
{
  public class AuctionCreatedFaultConsumer : IConsumer<Fault<AuctionCreated>>
  {
    private readonly ILoggerManager _logger;

    public AuctionCreatedFaultConsumer(ILoggerManager logger)
    {
      _logger = logger;
    }

    public async Task Consume(ConsumeContext<Fault<AuctionCreated>> context)
    {
      _logger.LogDebug("--> Consuming faulty auction creation...");

      var exception = context.Message.Exceptions.First();

      // Test fault consumption
      if (exception.ExceptionType == "System.ArgumentException")
      {
        context.Message.Message.Model = "FooBar";
        await context.Publish(context.Message.Message);
      }
      else
      {
        _logger.LogDebug("Not an argument exception - Update error dashboard somewhere");
      }
    }
  }
}
