using Carsties.AuctionService.BusinessLogic.Repositories;
using Carsties.AuctionService.Entities.Enums;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;

namespace Carsties.AuctionService.BusinessLogic.Consumers
{
  public class AuctionFinishedConsumer : IConsumer<AuctionFinished>
  {
    private readonly ILoggerManager _logger;
    private readonly IAuctionRepository _auctionRepository;

    public AuctionFinishedConsumer(ILoggerManager logger, IAuctionRepository auctionRepository)
    {
      _logger = logger;
      _auctionRepository = auctionRepository;
    }

    public async Task Consume(ConsumeContext<AuctionFinished> context)
    {
      _logger.LogInfo("--> Consuming auction finished");

      var auction = await _auctionRepository.GetAuctionByIdAsync(Guid.Parse(context.Message.AuctionId));

      if (context.Message.ItemSold)
      {
        auction.Winner = context.Message.Winner;
        auction.SoldAmount = context.Message.Amount;
      }

      auction.Status = auction.SoldAmount > auction.ReservePrice
        ? Status.Finished : Status.ReserveNotMet;

      await _auctionRepository.SaveChangesAsync();
    }
  }
}
