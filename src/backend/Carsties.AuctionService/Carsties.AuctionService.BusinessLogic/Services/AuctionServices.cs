using AutoMapper;
using Carsties.AuctionService.BusinessLogic.Repositories;
using Carsties.AuctionService.Entities.Dtos;
using Carsties.AuctionService.Entities.Models;
using Carsties.Shared.Common;
using Carsties.Shared.Common.Caching;
using Carsties.Shared.Common.Exceptions;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Common.ResponseHelpers;
using Carsties.Shared.Entities.Events;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;

namespace Carsties.AuctionService.BusinessLogic.Services
{
  public class AuctionServices : IAuctionServices
  {
    private readonly IAuctionRepository _auctionRepository;
    private readonly IMapper _mapper;
    private readonly ILoggerManager _logger;
    private readonly IPublishEndpoint _publishEndpoint;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ICacheWrapper _cache;
    private static readonly TypedCacheKey _allAuctionsCacheKey = TypedCacheKey.GetAllAuctionsCacheKey();

    public AuctionServices(IAuctionRepository auctionRepository, IMapper mapper, ILoggerManager logger, IPublishEndpoint publishEndpoint, IHttpContextAccessor httpContextAccessor, ICacheWrapper cache)
    {
      _auctionRepository = auctionRepository;
      _mapper = mapper;
      _logger = logger;
      _publishEndpoint = publishEndpoint;
      _httpContextAccessor = httpContextAccessor;
      _cache = cache;
    }

    public async Task<Result<AuctionDto>> CreateAuction(CreateAuctionDto createAuctionDto)
    {
      var auctionToCreate = _mapper.Map<Auction>(createAuctionDto);

      auctionToCreate.Seller = GetCurrentUserName();

      await _auctionRepository.CreateAuctionAsync(auctionToCreate);

      var auctionToCreateDto = _mapper.Map<AuctionDto>(auctionToCreate);

      var auctionToPublish = _mapper.Map<AuctionCreated>(auctionToCreateDto);

      // This must be put before SaveChangesAsync() as it is a part of DbContext transaction now (Outbox schema)
      await _publishEndpoint.Publish(auctionToPublish);

      var result = await _auctionRepository.SaveChangesAsync() > 0;

      if (!result)
      {
        _logger.LogError("Failed to create a new auction. Could not save the changes to the database.");
        return Result<AuctionDto>.Failure("Failed to create a new auction. Could not save the changes to the database.");
      }
      _logger.LogInfo($"Invalidating cache for key: {_allAuctionsCacheKey} from cache.", _allAuctionsCacheKey);

      await _cache.RemoveAsync(_allAuctionsCacheKey);

      return Result<AuctionDto>.Success(auctionToCreateDto);
    }

    public async Task<Result<IEnumerable<AuctionDto>>> GetAllAuctions(string date)
    {
      var cacheOptions = new DistributedCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromMinutes(20))
                .SetSlidingExpiration(TimeSpan.FromMinutes(2));

      var products = await _cache.GetOrSetAsync(
            _allAuctionsCacheKey,
            async () =>
            {
              _logger.LogInfo($"Cache missing. Fetching data for key: {_allAuctionsCacheKey} from database.", _allAuctionsCacheKey);
              IEnumerable<Auction> auctions;

              if (!string.IsNullOrEmpty(date))
              {
                auctions = await _auctionRepository
                  .Find(x => x.UpdatedAt.CompareTo(DateTime.Parse(date).ToUniversalTime()) > 0)
                  .OrderBy(x => x.Item.Make)
                  .ToListAsync();
              }
              else
              {
                auctions = await _auctionRepository.GetAllAuctionsAsync();
              }

              var auctionsDto = _mapper.Map<IEnumerable<AuctionDto>>(auctions);

              return Result<IEnumerable<AuctionDto>>.Success(auctionsDto);
            },
            cacheOptions)!;

      return products!;
    }

    public async Task<Result<AuctionDto>> GetAuctionById(Guid id)
    {
      var singleAuctionCacheKey = TypedCacheKey.GetSingleAuctionCacheKey(id);

      var auction = await _cache.GetOrSetAsync(singleAuctionCacheKey,
        async () =>
        {
          _logger.LogInfo($"Cache missing. Fetching data for key: {singleAuctionCacheKey} from database.", singleAuctionCacheKey);
          var auction = await _auctionRepository.GetAuctionByIdAsync(id) ?? throw new NotFoundException($"Auction with the id: {id} was not found.");

          return Result<AuctionDto>.Success(_mapper.Map<AuctionDto>(auction));
        });

      return auction;
    }

    public async Task<Result> UpdateAuction(Guid id, UpdateAuctionDto updateAuctionDto)
    {
      var cacheKey = TypedCacheKey.GetSingleAuctionCacheKey(id);

      var auctionToUpdate = await _auctionRepository.FindFirstOrDefaultAsync(condition: a => a.Id == id, trackChanges: true, includedProperties: a => a.Item) ?? throw new NotFoundException($"Auction with the id: {id} was not found.");

      if (auctionToUpdate.Seller != GetCurrentUserName())
      {
        throw new ForbiddenException("Current user is not the seller");
      }

      auctionToUpdate.Item.Make = updateAuctionDto.Make ?? auctionToUpdate.Item.Make;
      auctionToUpdate.Item.Model = updateAuctionDto.Model ?? auctionToUpdate.Item.Model;
      auctionToUpdate.Item.Color = updateAuctionDto.Color ?? auctionToUpdate.Item.Color;
      auctionToUpdate.Item.Mileage = updateAuctionDto.Mileage ?? auctionToUpdate.Item.Mileage;
      auctionToUpdate.Item.Year = updateAuctionDto.Year ?? auctionToUpdate.Item.Year;

      await _publishEndpoint.Publish(_mapper.Map<AuctionUpdated>(auctionToUpdate));

      var result = await _auctionRepository.SaveChangesAsync() > 0;

      if (!result)
      {
        _logger.LogError("Failed to create a new auction. Could not save the changes to the database.");
        return Result.Failure("Failed to create a new auction. Could not save the changes to the database.");
      }

      _logger.LogInfo($"Invalidating cache for key: {cacheKey} from cache.", cacheKey);

      await _cache.RemoveAsync(cacheKey);

      return Result.Success();
    }

    public async Task<Result> DeleteAuction(Guid id)
    {
      var cacheKey = TypedCacheKey.GetSingleAuctionCacheKey(id);

      var auctionToRemove = await _auctionRepository.FindFirstOrDefaultAsync(condition: a => a.Id == id) ?? throw new NotFoundException($"Auction with the id: {id} was not found.");

      if (auctionToRemove.Seller != GetCurrentUserName())
      {
        throw new ForbiddenException("Current user is not the seller");
      }

      _auctionRepository.Remove(auctionToRemove);

      await _publishEndpoint.Publish<AuctionDeleted>(new { Id = auctionToRemove.Id.ToString() });

      var result = await _auctionRepository.SaveChangesAsync() > 0;

      if (!result)
      {
        _logger.LogError("Failed to create a new auction. Could not save the changes to the database.");
        return Result.Failure("Failed to create a new auction. Could not save the changes to the database.");
      }

      _logger.LogInfo($"Invalidating cache for key: {cacheKey} from cache.", cacheKey);

      await _cache.RemoveAsync(cacheKey);

      return Result.Success();
    }

    private string GetCurrentUserName()
    {
      return _httpContextAccessor.HttpContext.User.Identity.Name;
    }
  }
}
