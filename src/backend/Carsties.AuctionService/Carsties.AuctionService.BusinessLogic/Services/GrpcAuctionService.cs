using Carsties.AuctionService.BusinessLogic.Repositories;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities;
using Grpc.Core;

namespace Carsties.AuctionService.BusinessLogic.Services;

public class GrpcAuctionService : GrpcAuction.GrpcAuctionBase
{
  private readonly IAuctionRepository _auctionRepository;
  private readonly ILoggerManager _logger;

  public GrpcAuctionService(IAuctionRepository auctionRepository, ILoggerManager logger)
  {
    _auctionRepository = auctionRepository;
    _logger = logger;
  }

  public override async Task<GrpcAuctionResponse> GetAuction(GetAuctionRequest request, ServerCallContext context)
  {
    _logger.LogInfo("==> Received Grpc request for auction");

    var auction = await _auctionRepository.GetAuctionByIdAsync(Guid.Parse(request.Id))
        ?? throw new RpcException(new Status(StatusCode.NotFound, "Not found"));

    var response = new GrpcAuctionResponse
    {
      Auction = new GrpcAuctionModel
      {
        AuctionEnd = auction.AuctionEnd.ToString(),
        Id = auction.Id.ToString(),
        ReservePrice = auction.ReservePrice,
        Seller = auction.Seller,
      },
    };
    return response;
  }
}
