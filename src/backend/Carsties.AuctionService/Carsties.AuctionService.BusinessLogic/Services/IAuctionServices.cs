using Carsties.AuctionService.Entities.Dtos;
using Carsties.Shared.Common.ResponseHelpers;

namespace Carsties.AuctionService.BusinessLogic.Services
{
  public interface IAuctionServices
  {
    Task<Result<IEnumerable<AuctionDto>>> GetAllAuctions(string date);

    Task<Result<AuctionDto>> GetAuctionById(Guid id);

    Task<Result<AuctionDto>> CreateAuction(CreateAuctionDto createAuctionDto);

    Task<Result> UpdateAuction(Guid id, UpdateAuctionDto updateAuctionDto);

    Task<Result> DeleteAuction(Guid id);
  }
}
