using AutoMapper;
using Carsties.AuctionService.Entities.Dtos;
using Carsties.Shared.Entities.Events;
using Carsties.AuctionService.Entities.Models;
using Carsties.Shared.Entities.Events;

namespace Carsties.AuctionService.BusinessLogic.Mappers;

public class MappingAuctionProfile : Profile
{
  public MappingAuctionProfile()
  {
    // Mapping APIs
    CreateMap<Auction, AuctionDto>().IncludeMembers(x => x.Item);
    CreateMap<Item, AuctionDto>();
    CreateMap<CreateAuctionDto, Auction>()
      .ForMember(d => d.Item, o => o.MapFrom(s => s));
    CreateMap<CreateAuctionDto, Item>();

    // Mapping message broker
    CreateMap<AuctionDto, AuctionCreated>();
    CreateMap<Auction, AuctionUpdated>().IncludeMembers(a => a.Item);
    CreateMap<Item, AuctionUpdated>();
  }
}
