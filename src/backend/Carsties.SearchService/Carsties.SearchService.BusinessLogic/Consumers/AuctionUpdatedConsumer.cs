using AutoMapper;
using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;
using MongoDB.Entities;

namespace Carsties.SearchService.BusinessLogic.Consumers
{
  public class AuctionUpdatedConsumer : IConsumer<AuctionUpdated>
  {
    private readonly IMapper _mapper;
    private readonly ILoggerManager _logger;

    public AuctionUpdatedConsumer(IMapper mapper, ILoggerManager logger)
    {
      _mapper = mapper;
      _logger = logger;
    }

    public async Task Consume(ConsumeContext<AuctionUpdated> context)
    {
      _logger.LogInfo("--> Consuming auction updated: " + context.Message.Id);

      var item = _mapper.Map<Item>(context.Message);

      var result = await DB.Update<Item>()
        .Match(a => a.ID == context.Message.Id)
        .ModifyOnly(
            x => new
            {
              x.Color,
              x.Make,
              x.Model,
              x.Year,
              x.Mileage,
            }, item).ExecuteAsync();

      if (!result.IsAcknowledged)
      {
        throw new MessageException(typeof(AuctionUpdated), "Problem updating the data in Search Service database");
      }
    }
  }
}
