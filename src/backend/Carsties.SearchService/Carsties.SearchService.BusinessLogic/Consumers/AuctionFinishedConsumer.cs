﻿using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;
using MongoDB.Entities;

namespace Carsties.SearchService.BusinessLogic.Consumers;

public class AuctionFinishedConsumer : IConsumer<AuctionFinished>
{
  private readonly ILoggerManager _logger;

  public AuctionFinishedConsumer(ILoggerManager logger)
  {
    _logger = logger;
  }

  public async Task Consume(ConsumeContext<AuctionFinished> context)
  {
    _logger.LogInfo("--> Consuming auction finished");

    var auction = await DB.Find<Item>().OneAsync(context.Message.AuctionId);

    if (context.Message.ItemSold)
    {
      auction.Winner = context.Message.Winner;
      auction.SoldAmount = (int)context.Message.Amount;
    }

    auction.Status = "Finished";

    await auction.SaveAsync();
  }
}
