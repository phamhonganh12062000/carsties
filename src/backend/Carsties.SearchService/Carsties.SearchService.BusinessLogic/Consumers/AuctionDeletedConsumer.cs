using AutoMapper;
using Carsties.Shared.Entities.Events;
using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Common.Logging;
using MassTransit;
using MongoDB.Entities;

namespace Carsties.SearchService.BusinessLogic.Consumers
{
  public class AuctionDeletedConsumer : IConsumer<AuctionDeleted>
  {
    private readonly IMapper _mapper;
    private readonly ILoggerManager _logger;

    public AuctionDeletedConsumer(IMapper mapper, ILoggerManager logger)
    {
      _mapper = mapper;
      _logger = logger;
    }

    public async Task Consume(ConsumeContext<AuctionDeleted> context)
    {
      _logger.LogInfo("--> Consuming auction deleted: " + context.Message.Id);

      var result = await DB.DeleteAsync<Item>(context.Message.Id);

      if (!result.IsAcknowledged)
      {
        throw new MessageException(typeof(AuctionDeleted), "Problem deleting auction");
      }
    }
  }
}
