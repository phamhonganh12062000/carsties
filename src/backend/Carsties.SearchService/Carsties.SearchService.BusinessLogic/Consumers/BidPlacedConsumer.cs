﻿using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using MassTransit;
using MongoDB.Entities;

namespace Carsties.SearchService.BusinessLogic;

public class BidPlacedConsumer : IConsumer<BidPlaced>
{
  private readonly ILoggerManager _logger;

  public BidPlacedConsumer(ILoggerManager logger)
  {
    _logger = logger;
  }

  public async Task Consume(ConsumeContext<BidPlaced> context)
  {
    _logger.LogInfo("--> Consuming bid placed");

    var auction = await DB.Find<Item>().OneAsync(context.Message.AuctionId);

    if (context.Message.BidStatus.Contains("Accepted")
        && context.Message.Amount > auction.CurrentHighBid)
    {
      auction.CurrentHighBid = context.Message.Amount;
      await auction.SaveAsync();
    }
  }
}
