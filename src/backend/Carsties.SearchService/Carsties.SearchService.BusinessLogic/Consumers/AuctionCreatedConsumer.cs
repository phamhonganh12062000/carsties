using AutoMapper;
using Carsties.Shared.Entities.Events;
using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Common.Logging;
using MassTransit;
using MongoDB.Entities;

namespace Carsties.SearchService.BusinessLogic.Consumers
{
  public class AuctionCreatedConsumer : IConsumer<AuctionCreated>
  {
    private readonly IMapper _mapper;
    private readonly ILoggerManager _logger;

    public AuctionCreatedConsumer(IMapper mapper, ILoggerManager logger)
    {
      _mapper = mapper;
      _logger = logger;
    }

    public async Task Consume(ConsumeContext<AuctionCreated> context)
    {
      _logger.LogInfo("--> Consuming auction created: " + context.Message.Id);

      var item = _mapper.Map<Item>(context.Message);

      if (item.Model == "Foo")
      {
        throw new ArgumentException("Cannot sell a car with a name of Foo");
      }

      await item.SaveAsync();
    }
  }
}
