using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Common.ResponseHelpers;
using Carsties.Shared.Entities.RequestParams;
using MongoDB.Entities;

namespace Carsties.SearchService.BusinessLogic.Services
{
  public class SearchServices : ISearchServices
  {
    public async Task<Result<object>> SearchItems(SearchParams searchParams)
    {
      var query = DB.PagedSearch<Item, Item>();

      if (!string.IsNullOrEmpty(searchParams.SearchTerm))
      {
        query.Match(Search.Full, searchParams.SearchTerm).SortByTextScore();
      }

      query = searchParams.OrderBy switch
      {
        "make" => query
          .Sort(x => x.Ascending(a => a.Make))
          .Sort(x => x.Ascending(a => a.Model)),
        "new" => query.Sort(x => x.Descending(a => a.CreatedAt)),
        _ => query.Sort(x => x.Ascending(a => a.AuctionEnd)),
      };

      query = searchParams.FilterBy switch
      {
        "finished" => query.Match(x => x.AuctionEnd < DateTime.UtcNow),
        "endingSoon" => query.Match(x => x.AuctionEnd < DateTime.UtcNow.AddHours(6) && x.AuctionEnd > DateTime.UtcNow),
        _ => query.Match(x => x.AuctionEnd > DateTime.UtcNow), // Find auctions that are still ongoing
      };

      if (!string.IsNullOrEmpty(searchParams.Seller))
      {
        query.Match(x => x.Seller == searchParams.Seller);
      }

      if (!string.IsNullOrEmpty(searchParams.Winner))
      {
        query.Match(x => x.Winner == searchParams.Winner);
      }

      query.PageNumber(searchParams.PageNumber);
      query.PageSize(searchParams.PageSize);

      var (results, totalCount, pageCount) = await query.ExecuteAsync();

      return Result<object>.Success(new
      {
        results,
        pageCount,
        totalCount,
      });
    }
  }
}
