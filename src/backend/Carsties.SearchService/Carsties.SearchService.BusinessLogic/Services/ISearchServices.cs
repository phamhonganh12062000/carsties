using Carsties.Shared.Common.ResponseHelpers;
using Carsties.Shared.Entities.RequestParams;
namespace Carsties.SearchService.BusinessLogic.Services
{
  public interface ISearchServices
  {
    Task<Result<object>> SearchItems(SearchParams searchParams);
  }
}
