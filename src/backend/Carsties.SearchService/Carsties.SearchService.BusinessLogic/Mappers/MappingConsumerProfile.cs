using AutoMapper;
using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Entities.Events;

namespace Carsties.SearchService.BusinessLogic.Mappers
{
  public class MappingConsumerProfile : Profile
  {
    public MappingConsumerProfile()
    {
      CreateMap<AuctionCreated, Item>();
      CreateMap<AuctionUpdated, Item>();
    }
  }
}
