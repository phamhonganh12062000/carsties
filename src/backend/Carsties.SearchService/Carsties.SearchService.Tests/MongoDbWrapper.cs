using Carsties.SearchService.Entities.Models;
using MongoDB.Driver;
using MongoDB.Entities;

namespace Carsties.SearchService.Tests
{
  public interface IMongoDbWrapper
  {
    IFindFluent<Item, Item> PagedSearch<T, TProjection>();
  }

  // public class MongoDbWrapper : IMongoDbWrapper
  // {
  //   public IFindFluent<Item, Item> PagedSearch<T, TProjection>()
  //   {
  //       return DB.PagedSearch<T, TProjection>();
  //   }
  // }
}
