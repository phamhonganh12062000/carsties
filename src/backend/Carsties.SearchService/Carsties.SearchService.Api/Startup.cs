using Serilog;
using MassTransit;
using Carsties.SearchService.BusinessLogic.Consumers;
using Carsties.Shared.Entities.ConfigurationModels;
using Carsties.Shared.Common.Logging;
using Carsties.SearchService.BusinessLogic.Services;

namespace Carsties.SearchService.Api
{
  public static class Startup
  {
    public static void ConfigureSerilog(this IHostBuilder hostBuilder)
    {
      hostBuilder.UseSerilog((context, services, configuration) => configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services)
        .Enrich.FromLogContext());
    }

    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddSingleton<ILoggerManager, LoggerManager>();
      services.AddScoped<ISearchServices, SearchServices>();
      services.AddMassTransit((opt) =>
      {
        opt.AddConsumersFromNamespaceContaining<AuctionCreatedConsumer>();

        opt.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter("search", false));

        var rabbitMqConfiguration = configuration.GetSection(nameof(RabbitMqConfiguration)).Get<RabbitMqConfiguration>()
        ?? throw new InvalidOperationException($"{nameof(RabbitMqConfiguration)} settings not found in configuration.");

        opt.UsingRabbitMq((context, config) =>
        {
          config.Host(rabbitMqConfiguration.Host, "/", (host) =>
          {
            host.Username(rabbitMqConfiguration.Username ?? "guest");
            host.Password(rabbitMqConfiguration.Password ?? "guest");
          });

          config.ReceiveEndpoint("search-auction-created", (e) =>
          {
            // Retry to get the message from the queue
            e.UseMessageRetry(r => r.Interval(5, 5)); // Hardcoding configs here
            e.ConfigureConsumer<AuctionCreatedConsumer>(context);
          });

          config.ConfigureEndpoints(context);
        });
      });
    }
  }
}
