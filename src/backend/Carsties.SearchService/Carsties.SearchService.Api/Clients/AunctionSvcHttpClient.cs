using Amazon.Runtime;
using Carsties.SearchService.Entities.Models;
using MongoDB.Entities;

namespace Carsties.SearchService.Api.Clients
{
  /// <summary>
  /// Keep the MongoDB synchronized with Postgresql of the auction service
  /// </summary>
  public class AuctionServiceHttpClient
  {
    private readonly HttpClient _httpClient;
    private readonly IConfiguration _config;

    public AuctionServiceHttpClient(HttpClient httpClient, IConfiguration config)
    {
      _httpClient = httpClient;
      _config = config;
    }

    /// <summary>
    /// Find most recently updated items in MongoDb
    /// Then fetch items from the auction service that have been updated since that timestamp.
    /// </summary>
    /// <returns></returns>
    public async Task<List<Item>> SyncItemsFromAuctionService()
    {
      // Get the latest updated timestamp
      var lastUpdated = await DB.Find<Item, string>()
        .Sort(x => x.Descending(x => x.UpdatedAt))
        .Project(x => x.UpdatedAt.ToString()) // Passes along the documents with the requested fields to the next stage in the pipeline.
        .ExecuteFirstAsync();

      // Asks the auction service to return all items that have been updated since lastUpdated
      return await _httpClient.GetFromJsonAsync<List<Item>>(_config["AuctionServiceUrl"] + "/api/auctions?date=" + lastUpdated);
    }
  }
}
