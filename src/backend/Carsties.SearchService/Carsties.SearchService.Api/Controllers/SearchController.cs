using Carsties.SearchService.BusinessLogic.Services;
using Carsties.Shared.Common.Controllers;
using Carsties.Shared.Entities.RequestParams;
using Microsoft.AspNetCore.Mvc;

namespace Carsties.SearchService.Api.Controllers
{
  public class SearchController : BaseController
  {
    private readonly ISearchServices _searchService;

    public SearchController(ISearchServices searchService)
    {
      _searchService = searchService;
    }

    [HttpGet]
    public async Task<IActionResult> SearchItems([FromQuery]SearchParams searchParams)
    {
      return HandleResult(await _searchService.SearchItems(searchParams));
    }
  }
}
