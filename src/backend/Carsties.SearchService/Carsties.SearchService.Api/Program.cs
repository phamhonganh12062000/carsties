using Carsties.SearchService.Api;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Common.Exceptions;
using Serilog;
using Serilog.Events;
using Carsties.SearchService.Api.Clients;
using Microsoft.AspNetCore.Mvc;
using Polly;
using Polly.Extensions.Http;
using System.Net;

var builder = WebApplication.CreateBuilder(args);
var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{environment}.json", optional: true)
        .AddEnvironmentVariables();

Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        .Enrich.FromLogContext()
        .CreateBootstrapLogger();

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Enable custom responses
builder.Services.Configure<ApiBehaviorOptions>(opts =>
{
  opts.SuppressModelStateInvalidFilter = true;
});
builder.Services.AddControllers();
builder.Services.AddAutoMapper(typeof(Carsties.SearchService.BusinessLogic.AssemblyReference).Assembly);
builder.Services.AddHttpClient<AuctionServiceHttpClient>().AddPolicyHandler(GetPolicy());

builder.Host.ConfigureSerilog();
builder.Services.ConfigureServices(builder.Configuration);

var app = builder.Build();

app.UseSerilogRequestLogging();
var logger = app.Services.GetRequiredService<ILoggerManager>();
app.ConfigureGlobalExceptionHandler(logger);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseHttpsRedirection();

// Configure the HTTP request pipeline.
app.UseAuthorization();

app.MapControllers();

// Run search service even when auction service is down
app.Lifetime.ApplicationStarted.Register(async () =>
{
  try
  {
    await DbInitializer.InitDb(app, logger);
  }
  catch (Exception)
  {
    throw;
  }
});

app.Run();

// Retry fetching data from auction service
static IAsyncPolicy<HttpResponseMessage> GetPolicy()
  => HttpPolicyExtensions
      .HandleTransientHttpError()
      .OrResult((message) => message.StatusCode == HttpStatusCode.NotFound)
      .WaitAndRetryForeverAsync(_ => TimeSpan.FromSeconds(5));
