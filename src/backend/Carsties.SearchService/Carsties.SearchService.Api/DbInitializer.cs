using Carsties.SearchService.Api.Clients;
using Carsties.SearchService.Entities.Models;
using Carsties.Shared.Common.Logging;
using MongoDB.Driver;
using MongoDB.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Carsties.SearchService.Api
{
  public class DbInitializer
  {
    public static async Task InitDb(WebApplication app, ILoggerManager logger)
    {
      await DB.InitAsync("SearchDb", MongoClientSettings.FromConnectionString(app.Configuration.GetConnectionString("MongoDbConnection")));

      await DB.Index<Item>()
        .Key(x => x.Make, KeyType.Text)
        .Key(x => x.Model, KeyType.Text)
        .Key(x => x.Color, KeyType.Text)
        .CreateAsync();

      var count = await DB.CountAsync<Item>();

      // if (count == 0)
      // {
      //   logger.LogDebug("No data - Attempting to seed");

      //   UpdateFakeDataDate("Data/auctions.json");
      //   var itemsToSeed = await File.ReadAllTextAsync("Data/auctions.json");

      //   var itemsToSeedDeserialized = System.Text.Json.JsonSerializer.Deserialize<List<Item>>(itemsToSeed, new JsonSerializerOptions
      //   {
      //     PropertyNameCaseInsensitive = true,
      //   });

      //   await DB.SaveAsync(itemsToSeedDeserialized);
      // }

      using var scope = app.Services.CreateScope();

      var auctionHttpClient = scope.ServiceProvider.GetRequiredService<AuctionServiceHttpClient>();

      var items = await auctionHttpClient.SyncItemsFromAuctionService();

      logger.LogDebug(items.Count + " returned from the auction service");

      if (items.Count > 0)
      {
        await DB.SaveAsync(items);
      }
    }

    private static void UpdateFakeDataDate(string path)
    {
      var json = File.ReadAllText(path);
      var jArrObj = JArray.Parse(json);

      foreach (JObject obj in jArrObj)
      {
        obj["auctionEnd"] = DateTime.UtcNow.AddDays(180).ToString("o");
      }

      string output = JsonConvert.SerializeObject(jArrObj, Formatting.Indented);
      File.WriteAllText(path, output);
    }
  }
}
