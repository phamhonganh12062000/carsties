﻿using Carsties.Shared.Common.Logging;
using Microsoft.Extensions.Options;

namespace Carsties.EmailService;

public interface IEmailSenderFactory
{
  void SendEmail(Message message);

  Task SendEmailAsync(Message message);
}

public class EmailSenderFactory : IEmailSenderFactory
{
  private readonly IEmailSenderFactory _senderInstance;
  private readonly ILoggerManager _logger;

  public EmailSenderFactory(IOptions<EmailConfiguration> smtpConfiguration, ILoggerManager logger)
  {
    _senderInstance = null;
    _logger = logger;

    var smtp = new EmailSmtp(smtpConfiguration, logger);

    if (smtp.IsValid)
    {
      _senderInstance = smtp;
    }
    else
    {
      _logger.LogError("SMTP email provider selected but no valid SMTP settings configured.");
    }
  }

  public void SendEmail(Message message)
  {
    if (_senderInstance != null)
    {
      _senderInstance.SendEmail(message);
    }
  }

  public async Task SendEmailAsync(Message message)
  {
    if (_senderInstance != null)
    {
      await _senderInstance.SendEmailAsync(message);
    }
  }
}
