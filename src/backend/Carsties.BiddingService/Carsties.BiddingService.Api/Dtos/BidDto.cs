namespace Carsties.BiddingService.Api.Dtos
{
  public record BidDto
  {
    public string Id { get; init; }
    public string AuctionId { get; init; }
    public string Bidder { get; init; }
    public DateTime BidDate { get; init; }
    public int Amount { get; init; }
    public string BidStatus { get; init; }
  }
}
