namespace Carsties.BiddingService.Api.Dtos
{
  public record CreateBidDto
  {
    public string AuctionId { get; init; }
    public int Amount { get; init; }
  }
}
