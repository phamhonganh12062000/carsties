using AutoMapper;
using Carsties.BiddingService.Api.Dtos;
using Carsties.BiddingService.Api.Models;
using Carsties.Shared.Entities.Events;

namespace Carsties.BiddingService.Api.Mappers
{
  public class MappingBidingProfile : Profile
  {
    public MappingBidingProfile()
    {
      CreateMap<Bid, BidDto>();
      CreateMap<Bid, BidPlaced>();
      CreateMap<CreateBidDto, Bid>();
    }
  }
}
