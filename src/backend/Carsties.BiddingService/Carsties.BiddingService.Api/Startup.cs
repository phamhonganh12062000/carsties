using Carsties.BiddingService.Api.Consumers;
using Carsties.BiddingService.Api.Jobs;
using Carsties.BiddingService.Api.Repositories;
using Carsties.BiddingService.Api.Services;
using Carsties.Shared.Common.ActionFilters;
using Carsties.Shared.Common.Jobs;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Common.Services;
using Carsties.Shared.Entities.ConfigurationModels;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Serilog;

namespace Carsties.BiddingService.Api
{
  public static class Startup
  {
    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
      services.Configure<RabbitMqConfiguration>(configuration.GetSection(nameof(RabbitMqConfiguration)));
      services.Configure<CronJobConfiguration>(configuration.GetSection(nameof(CronJobConfiguration)));
      services.AddSingleton<ILoggerManager, LoggerManager>();
      services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
      services.AddHostedService<BackgroundTaskWorker>();
      services.AddScoped<GrpcAuctionClient>();
      services.AddScoped<IBiddingServices, BiddingServices>();
      services.AddScoped<IBiddingRepository, BiddingRepository>();
      services.AddScoped<ValidationFilterAttribute>();
      services.AddControllers();
      services.AddEndpointsApiExplorer();
      services.AddSwaggerGen();
      services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
      services.AddMassTransit((opt) =>
      {
        var rabbitMqConfiguration = configuration.GetSection(nameof(RabbitMqConfiguration)).Get<RabbitMqConfiguration>()
        ?? throw new InvalidOperationException($"{nameof(RabbitMqConfiguration)} settings not found in configuration.");

        opt.AddConsumersFromNamespaceContaining<AuctionCreatedConsumer>();

        opt.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter("bids", false));

        opt.UsingRabbitMq((context, config) =>
        {
          config.Host(rabbitMqConfiguration.Host, "/", (host) =>
          {
            host.Username(rabbitMqConfiguration.Username ?? "guest");
            host.Password(rabbitMqConfiguration.Password ?? "guest");
          });

          config.ConfigureEndpoints(context);
        });
      });

      services.AddAuthentication((opts) =>
      {
        opts.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
      }).AddJwtBearer((opts) =>
      {
        opts.Authority = configuration["IdentityServiceUrl"];
        opts.RequireHttpsMetadata = false;
        opts.TokenValidationParameters.ValidateAudience = false;
        opts.TokenValidationParameters.NameClaimType = "username";
      });

      // services.AddQuartz(q =>
      // {
      //   var quartzConfiguration = configuration.GetSection(nameof(QuartzConfiguration)).Get<QuartzConfiguration>()
      //   ?? throw new InvalidOperationException($"{nameof(QuartzConfiguration)} settings not found in configuration.");

      //   var jobKey = new JobKey(nameof(CheckAuctionsJob));
      //   q.AddJob<CheckAuctionsJob>(opts => opts.WithIdentity(jobKey)); // Default as singleton lifecycle

      //   q.AddTrigger(opts => opts
      //       .ForJob(jobKey)
      //       .WithIdentity("CheckAuctionsJob-trigger")
      //       .WithCronSchedule(quartzConfiguration.TriggerTime));
      // });

      // services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);

      services.AddCronJob<CheckAuctionsJob>(configuration
      .GetSection(nameof(CronJobConfiguration))
      .Get<CronJobConfiguration>()
      .TriggerTime);
      services.AddHttpClient();
    }

    public static void ConfigureSerilog(this IHostBuilder hostBuilder)
    {
      hostBuilder.UseSerilog((context, services, configuration) => configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services)
        .Enrich.FromLogContext());
    }
  }
}
