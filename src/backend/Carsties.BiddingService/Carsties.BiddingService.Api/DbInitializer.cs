using MongoDB.Driver;
using MongoDB.Entities;

namespace Carsties.BiddingService.Api
{
  public class DbInitializer
  {
    public static async Task InitDb(WebApplication app)
    {
      await DB.InitAsync("BiddingDb", MongoClientSettings.FromConnectionString(app.Configuration.GetConnectionString("MongoDbConnection")));
    }
  }
}
