using Carsties.BiddingService.Api.Dtos;
using Carsties.BiddingService.Api.Services;
using Carsties.Shared.Common.ActionFilters;
using Carsties.Shared.Common.Controllers;
using Carsties.Shared.Common.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Carsties.BiddingService.Api.Controllers
{
  public class BiddingController : BaseController
  {
    private readonly IBiddingServices _biddingServices;
    private readonly IBackgroundTaskQueue _backgroundTaskQueue;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public BiddingController(IBiddingServices biddingServices, IBackgroundTaskQueue backgroundTaskQueue, IServiceScopeFactory serviceScopeFactory)
    {
      _biddingServices = biddingServices;
      _backgroundTaskQueue = backgroundTaskQueue;
      _serviceScopeFactory = serviceScopeFactory;
    }

    [HttpPost("check-auctions")]
    public async Task<IActionResult> CheckAuctions()
    {
      try
      {
        _backgroundTaskQueue.Enqueue(async (token) =>
        {
          // Background tasks may run outside the scope of an HTTP request
          // We need to create a new instance of BiddingService for the duration of the background task
          using var scope = _serviceScopeFactory.CreateScope();
          var biddingService = scope.ServiceProvider.GetRequiredService<IBiddingServices>();
          await biddingService.CheckAuctions(token);
        });
        return Ok();
      }
      catch (Exception)
      {
        throw;
      }
    }

    [Authorize]
    [HttpPost("place-bid")]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    public async Task<IActionResult> PlaceBid([FromBody] CreateBidDto createBidDto)
    {
      return HandleResult(await _biddingServices.PlaceBid(createBidDto, GetCurrentUserName()));
    }

    [HttpGet("{auctionId}")]
    public async Task<IActionResult> GetBidsForAuction(string auctionId)
    {
      return HandleResult(await _biddingServices.GetBidsForAuction(auctionId));
    }

    private string GetCurrentUserName()
    {
      return User.Identity.Name;
    }
  }
}
