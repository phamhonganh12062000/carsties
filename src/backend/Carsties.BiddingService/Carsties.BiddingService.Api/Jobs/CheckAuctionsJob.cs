using Carsties.Shared.Common.Jobs;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.ConfigurationModels;
using Microsoft.Extensions.Options;

namespace Carsties.BiddingService.Api.Jobs
{
  public class CheckAuctionsJob : ICronJob
  {
    private readonly IHttpClientFactory _clientFactory;
    private readonly ILoggerManager _logger;
    private readonly IOptions<CronJobConfiguration> _configuration;

    public CheckAuctionsJob(IHttpClientFactory clientFactory, ILoggerManager logger, IOptions<CronJobConfiguration> configuration)
    {
      _clientFactory = clientFactory;
      _logger = logger;
      _configuration = configuration;
    }

    public async Task Run(CancellationToken token = default)
    {
      var client = _clientFactory.CreateClient();
      var response = await client.PostAsync($"{_configuration.Value.ServiceUrl}/api/bidding/check-auctions", null);

      if (response.IsSuccessStatusCode)
      {
        _logger.LogInfo("Job succeeded!");
      }
      else
      {
        _logger.LogError("Job failed!");
      }
    }
  }
}
