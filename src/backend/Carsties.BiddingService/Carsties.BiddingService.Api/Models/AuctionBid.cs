using MongoDB.Entities;

namespace Carsties.BiddingService.Api.Models
{
  public class AuctionBid : Entity
  {
    public DateTime AuctionEnd { get; set; }
    public string Seller { get; set; }
    public int ReservePrice { get; set; }
    public bool Finished { get; set; }
  }
}
