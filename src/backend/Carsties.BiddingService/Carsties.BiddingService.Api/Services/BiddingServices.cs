using AutoMapper;
using Carsties.BiddingService.Api.Dtos;
using Carsties.BiddingService.Api.Enums;
using Carsties.BiddingService.Api.Models;
using Carsties.BiddingService.Api.Repositories;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Common.ResponseHelpers;
using Carsties.Shared.Entities.Events;
using MassTransit;
using MongoDB.Entities;

namespace Carsties.BiddingService.Api.Services
{
  public class BiddingServices : IBiddingServices
  {
    private readonly ILoggerManager _logger;
    private readonly IPublishEndpoint _publishEndpoint;
    private readonly IBiddingRepository _biddingRepository;
    private readonly GrpcAuctionClient _grpcClient;
    private readonly IMapper _mapper;

    public BiddingServices(ILoggerManager logger, IPublishEndpoint publishEndpoint, IBiddingRepository biddingRepository, GrpcAuctionClient grpcClient, IMapper mapper)
    {
      _logger = logger;
      _publishEndpoint = publishEndpoint;
      _biddingRepository = biddingRepository;
      _grpcClient = grpcClient;
      _mapper = mapper;
    }

    public async Task CheckAuctions(CancellationToken cancellationToken = default)
    {
      var finishedAuctions = await _biddingRepository.GetFinishedAuctions(cancellationToken);

      if (finishedAuctions.Count == 0)
      {
        return;
      }

      _logger.LogInfo($"==> Found {finishedAuctions.Count} auctions that have completed");

      foreach (var auction in finishedAuctions)
      {
        auction.Finished = true;

        await auction.SaveAsync(null, cancellationToken);

        var winningBid = await _biddingRepository.GetWinningBid(auction.ID, cancellationToken);

        await _publishEndpoint.Publish(
          new AuctionFinished
          {
            ItemSold = winningBid != null,
            AuctionId = auction.ID,
            Winner = winningBid?.Bidder,
            Amount = winningBid?.Amount,
            Seller = auction.Seller,
          },
          cancellationToken);
      }
    }

    public async Task<Result<BidDto>> PlaceBid(CreateBidDto createBidDto, string bidderName)
    {
      var auction = await _biddingRepository.FindAuctionBid(createBidDto.AuctionId);

      if (auction is null)
      {
        // Use grpc as a fallback
        auction = await _grpcClient.GetAuctionBidAsync(createBidDto.AuctionId);

        if (auction is null)
        {
          return Result<BidDto>.Failure("Cannot accept bids on this auction at this time");
        }
      }

      if (auction.Seller == bidderName)
      {
        return Result<BidDto>.Failure("You cannot bid on your own auction");
      }

      var bid = new Bid
      {
        Amount = createBidDto.Amount,
        AuctionId = createBidDto.AuctionId,
        Bidder = bidderName,
      };

      if (auction.AuctionEnd < DateTime.UtcNow)
      {
        bid.BidStatus = BidStatus.Finished;
      }
      else
      {
        var highestBid = await _biddingRepository.FindHighestBid(createBidDto.AuctionId);

        UpdateBidStatus(highestBid, bid, auction);
      }

      await DB.SaveAsync(bid);

      await _publishEndpoint.Publish(_mapper.Map<BidPlaced>(bid));

      return Result<BidDto>.Success(_mapper.Map<BidDto>(bid));
    }

    private static void UpdateBidStatus(Bid highestBid, Bid newBid, AuctionBid auctionBid)
    {
      if ((highestBid != null && newBid.Amount > highestBid.Amount)
        || highestBid is null)
      {
        newBid.BidStatus = newBid.Amount > auctionBid.ReservePrice
          ? BidStatus.Accepted
          : BidStatus.AcceptedBelowReserve;
      }

      if (highestBid != null && newBid.Amount <= highestBid.Amount)
      {
        newBid.BidStatus = BidStatus.TooLow;
      }
    }

    public async Task<Result<List<BidDto>>> GetBidsForAuction(string auctionId)
    {
      var bids = await _biddingRepository.GetBidsForAuction(auctionId);

      return Result<List<BidDto>>.Success(_mapper.Map<List<BidDto>>(bids));
    }
  }
}
