using Carsties.BiddingService.Api.Models;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities;
using Grpc.Net.Client;

namespace Carsties.BiddingService.Api.Services;

public class GrpcAuctionClient
{
  private readonly ILoggerManager _logger;
  private readonly IConfiguration _configuration;

  public GrpcAuctionClient()
  {
    // For testing purposes
  }

  public GrpcAuctionClient(ILoggerManager logger, IConfiguration configuration)
  {
    _logger = logger;
    _configuration = configuration;
  }

  public async Task<AuctionBid> GetAuctionBidAsync(string id)
  {
    _logger.LogInfo("Calling GRPC Service");
    var channel = GrpcChannel.ForAddress(_configuration["GrpcAuction"]);
    var client = new GrpcAuction.GrpcAuctionClient(channel);
    var request = new GetAuctionRequest { Id = id };

    try
    {
      var reply = await client.GetAuctionAsync(request);
      var auction = new AuctionBid
      {
        ID = reply.Auction.Id,
        AuctionEnd = DateTime.Parse(reply.Auction.AuctionEnd),
        Seller = reply.Auction.Seller,
        ReservePrice = reply.Auction.ReservePrice,
      };

      return auction;
    }
    catch (Exception ex)
    {
      _logger.LogError(ex, "Could not call GRPC Server");
      throw;
    }
  }
}
