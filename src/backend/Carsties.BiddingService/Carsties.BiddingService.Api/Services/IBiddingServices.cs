using Carsties.BiddingService.Api.Dtos;
using Carsties.Shared.Common.ResponseHelpers;

namespace Carsties.BiddingService.Api.Services
{
  public interface IBiddingServices
  {
    Task CheckAuctions(CancellationToken cancellationToken = default);

    Task<Result<BidDto>> PlaceBid(CreateBidDto createBidDto, string bidderName);

    Task<Result<List<BidDto>>> GetBidsForAuction(string auctionId);
  }
}
