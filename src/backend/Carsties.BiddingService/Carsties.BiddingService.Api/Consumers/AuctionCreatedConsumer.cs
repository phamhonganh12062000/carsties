using Carsties.BiddingService.Api.Models;
using Carsties.Shared.Entities.Events;
using MassTransit;
using MongoDB.Entities;

namespace Carsties.BiddingService.Api.Consumers
{
  public class AuctionCreatedConsumer : IConsumer<AuctionCreated>
  {
    public async Task Consume(ConsumeContext<AuctionCreated> context)
    {
      var auction = new AuctionBid
      {
        ID = context.Message.Id.ToString(),
        Seller = context.Message.Seller,
        AuctionEnd = context.Message.AuctionEnd,
        ReservePrice = context.Message.ReservePrice,
      };
      await auction.SaveAsync();
    }
  }
}
