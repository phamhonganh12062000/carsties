namespace Carsties.BiddingService.Api.Enums
{
  public enum BidStatus
  {
    Accepted,
    AcceptedBelowReserve,
    TooLow,
    Finished
  }
}
