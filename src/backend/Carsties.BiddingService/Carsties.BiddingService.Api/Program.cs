using Carsties.BiddingService.Api;
using Carsties.Shared.Common.Exceptions;
using Carsties.Shared.Common.Logging;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{environment}.json", optional: true)
        .AddEnvironmentVariables();

builder.Services.ConfigureServices(builder.Configuration);
builder.Host.ConfigureSerilog();

var app = builder.Build();
app.UseSerilogRequestLogging();
var logger = app.Services.GetRequiredService<ILoggerManager>();
app.ConfigureGlobalExceptionHandler(logger);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseAuthorization();
app.MapControllers();

await DbInitializer.InitDb(app);

app.Run();
