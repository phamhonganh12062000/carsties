using Carsties.BiddingService.Api.Enums;
using Carsties.BiddingService.Api.Models;
using MongoDB.Entities;

namespace Carsties.BiddingService.Api.Repositories
{
  public class BiddingRepository : IBiddingRepository
  {
    public async Task<AuctionBid> FindAuctionBid(string auctionId)
    {
      return await DB.Find<AuctionBid>().OneAsync(auctionId);
    }

    public async Task<Bid> FindHighestBid(string auctionId)
    {
      return await DB.Find<Bid>()
        .Match(a => a.AuctionId == auctionId)
        .Sort(b => b.Descending(x => x.Amount))
        .ExecuteFirstAsync();
    }

    public async Task<List<Bid>> GetBidsForAuction(string auctionId)
    {
      return await DB.Find<Bid>()
        .Match(a => a.AuctionId == auctionId)
        .Sort(b => b.Descending(a => a.BidDate))
        .ExecuteAsync();
    }

    public async Task<List<AuctionBid>> GetFinishedAuctions(CancellationToken cancellationToken)
    {
      return await DB.Find<AuctionBid>()
          .Match(x => x.AuctionEnd <= DateTime.UtcNow && !x.Finished)
          .ExecuteAsync(cancellationToken);
    }

    public async Task<Bid> GetWinningBid(string auctionId, CancellationToken cancellationToken)
    {
      return await DB.Find<Bid>()
          .Match(b => b.AuctionId == auctionId && b.BidStatus == BidStatus.Accepted)
          .Sort(x => x.Descending(s => s.Amount))
          .ExecuteFirstAsync(cancellationToken);
    }
  }
}
