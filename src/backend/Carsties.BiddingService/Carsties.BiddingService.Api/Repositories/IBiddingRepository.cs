using Carsties.BiddingService.Api.Models;

namespace Carsties.BiddingService.Api.Repositories
{
  public interface IBiddingRepository
  {
    Task<List<AuctionBid>> GetFinishedAuctions(CancellationToken cancellationToken);

    Task<Bid> GetWinningBid(string auctionId, CancellationToken cancellationToken);

    Task<AuctionBid> FindAuctionBid(string auctionId);

    Task<Bid> FindHighestBid(string auctionId);

    Task<List<Bid>> GetBidsForAuction(string auctionId);
  }
}
