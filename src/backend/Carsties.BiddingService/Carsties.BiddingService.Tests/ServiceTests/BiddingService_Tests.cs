using AutoMapper;
using Carsties.BiddingService.Api.Enums;
using Carsties.BiddingService.Api.Models;
using Carsties.BiddingService.Api.Repositories;
using Carsties.BiddingService.Api.Services;
using Carsties.Shared.Common.Logging;
using Carsties.Shared.Entities.Events;
using Carsties.Shared.Tests;
using MassTransit;
using MongoDB.Entities;
using Moq;
using Xunit;

namespace Carsties.BiddingService.Tests.ServiceTests;

public class BiddingService_Tests
{
  private readonly SharedFixtureSetup _sharedFixtureSetup;
  private readonly Mock<ILoggerManager> _loggerMock;
  private readonly Mock<IPublishEndpoint> _publishEndpointMock;
  private readonly Mock<IBiddingRepository> _biddingRepositoryMock;
  private readonly BiddingServices _biddingServices;
  // private readonly Mock<GrpcAuctionClient> _grpcAuctionClientMock;
  private readonly Mock<IMapper> _mapperMock;
  private readonly GrpcAuctionClient _grpcAuctionClientMock;

  public BiddingService_Tests()
  {
    _sharedFixtureSetup = new SharedFixtureSetup();
    _loggerMock = new Mock<ILoggerManager>();
    _publishEndpointMock = new Mock<IPublishEndpoint>();
    _biddingRepositoryMock = new Mock<IBiddingRepository>();
    _grpcAuctionClientMock = new GrpcAuctionClient();
    _mapperMock = new Mock<IMapper>();
    _biddingServices = new BiddingServices(_loggerMock.Object, _publishEndpointMock.Object, _biddingRepositoryMock.Object, _grpcAuctionClientMock, _mapperMock.Object);
  }

  [Fact]
  public async Task CheckAuctions_NoFinishedAuctionsFound_DoesNotLogOrPublish()
  {
    // ARRANGE
    _biddingRepositoryMock.Setup(repo => repo.GetFinishedAuctions(It.IsAny<CancellationToken>()))
        .ReturnsAsync(new List<AuctionBid>()); // No auctions

    // ACT
    await _biddingServices.CheckAuctions();

    // ASSERT
    _loggerMock.Verify(l => l.LogInfo(It.IsAny<string>()), Times.Never);
    _publishEndpointMock.Verify(p => p.Publish(It.IsAny<AuctionFinished>(), It.IsAny<CancellationToken>()), Times.Never);
  }

  // [Fact]
  // public async Task CheckAuctions_AuctionsFound_LogsAndPublishesEvent()
  // {
  //   // ARRANGE
  //   var auction = new AuctionBid { ID = "123", AuctionEnd = DateTime.UtcNow.AddHours(-1), Finished = false, Seller = "Seller1" };
  //   var bid = new Bid { AuctionId = "123", Bidder = "Bidder1", Amount = 100, BidStatus = BidStatus.Accepted };

  //   _biddingRepositoryMock.Setup(repo => repo.GetFinishedAuctions(It.IsAny<CancellationToken>()))
  //       .ReturnsAsync(new List<AuctionBid> { auction });

  //   _biddingRepositoryMock.Setup(repo => repo.GetWinningBid(auction.ID, It.IsAny<CancellationToken>()))
  //       .ReturnsAsync(bid);

  //   // ACT
  //   await _biddingServices.CheckAuctions();

  //   // ASSERT
  //   _loggerMock.Verify(l => l.LogInfo($"==> Found 1 auctions that have completed"), Times.Once);
  //   _publishEndpointMock.Verify(p => p.Publish(It.Is<AuctionFinished>(e => e.AuctionId == "123" && e.ItemSold && e.Winner == "Bidder1" && e.Amount == 100), It.IsAny<CancellationToken>()), Times.Once);
  // }

  // [Fact]
  // public async Task CheckAuctions_NoWinningBid_PublishesEventWithNoWinner()
  // {
  //   // ARRANGE
  //   var auction = new AuctionBid { ID = "124", AuctionEnd = DateTime.UtcNow.AddHours(-1), Finished = false, Seller = "Seller2" };

  //   _biddingRepositoryMock.Setup(repo => repo.GetFinishedAuctions(It.IsAny<CancellationToken>()))
  //       .ReturnsAsync(new List<AuctionBid> { auction });

  //   _biddingRepositoryMock.Setup(repo => repo.GetWinningBid(auction.ID, It.IsAny<CancellationToken>()))
  //       .ReturnsAsync((Bid)null); // No bid

  //   // ACT
  //   await _biddingServices.CheckAuctions();

  //   // ASSERT
  //   _publishEndpointMock.Verify(p => p.Publish(It.Is<AuctionFinished>(e => e.AuctionId == "124" && !e.ItemSold && e.Winner == null), It.IsAny<CancellationToken>()), Times.Once);
  // }
}
