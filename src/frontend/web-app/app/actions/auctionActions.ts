"use server";

import { revalidatePath } from "next/cache";
import { FieldValues } from "react-hook-form";
import { fetchWrapper } from "../../lib/fetchWrapper";
import { Auction, PagedResult } from "../types";

export async function getData(query: string): Promise<PagedResult<Auction>> {
    return await fetchWrapper.get(`search/${query}`);
}

export async function updateAuctionTest() {
    const data = {
        make: "Ford",
        model: "Updated",
        color: "red",
        mileage: Math.floor(Math.random() * 100000) + 1,
        year: 1999,
    };

    // TODO: Hard-coded auction for testing, delete later
    return await fetchWrapper.post(
        "http://localhost:6001/auctions/afbee524-5972-4075-8800-7d1f9d7b0a0c",
        data
    );
}

export async function getDetailedViewData(id: string): Promise<Auction> {
  return await fetchWrapper.get(`auctions/${id}`);
}

export async function createAuction(data: FieldValues) {
    return await fetchWrapper.post("auctions", data);
}

export async function updateAuction(data: FieldValues, id: string) {
    const response = await fetchWrapper.put(`auctions/${id}`, data);
    revalidatePath(`/auctions/${id}`); // Purge cached data for specific path
    return response;
}

export async function deleteAuction(id: string) {
    return await fetchWrapper.del(`auctions/${id}`);
}
