import { getCurrentUser } from "../actions/authActions";
import UserActionsDrawer from "../auctions/UserActionsDrawer";
import LoginButton from "./LoginButton";
import Logo from "./Logo";
import NotificationButton from "./NotificationButton";
import Search from "./Search";
import UserOptions from "./UserOptions";

export default async function Navbar() {
    const user = await getCurrentUser();
    return (
        <div className="navbar bg-base-300">
            <UserActionsDrawer user={user!} />
            <Logo />
            <Search />
            <div className="navbar-end">
                <NotificationButton />
                {user ? <UserOptions user={user} /> : <LoginButton />}
            </div>
        </div>
    );
}
