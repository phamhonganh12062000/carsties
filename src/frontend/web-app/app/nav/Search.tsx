"use client";

import { usePathname, useRouter } from "next/navigation";
import { useParamsStore } from "../hooks/useParamsStore";

export default function Search() {
    const setParams = useParamsStore((state) => state.setParams);
    const setSearchValue = useParamsStore((state) => state.setSearchValue);
    const searchValue = useParamsStore((state) => state.searchValue);

    const router = useRouter();
    const pathName = usePathname();

    function onChange(event: any) {
        setSearchValue(event.target.value);
    }

    function search() {
        if (pathName !== "/") router.push("/");
        setParams({ searchTerm: searchValue });
    }
    return (
        <div className="flex-none gap-2">
            <div>
                <label className="input input-bordered flex items-center gap-2 w-96">
                    <input
                        onKeyDown={(e: any) => {
                            if (e.key === "Enter") search();
                        }}
                        value={searchValue}
                        onChange={onChange}
                        type="text"
                        placeholder="Search for cars by make, model or color"
                        className="grow"
                    />
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 16 16"
                        fill="currentColor"
                        className="w-4 h-4 opacity-70"
                    >
                        <path
                            fillRule="evenodd"
                            d="M9.965 11.026a5 5 0 1 1 1.06-1.06l2.755 2.754a.75.75 0 1 1-1.06 1.06l-2.755-2.754ZM10.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z"
                            clipRule="evenodd"
                        />
                    </svg>
                </label>
            </div>
        </div>
    );
}
