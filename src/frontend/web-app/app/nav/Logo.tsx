"use client";

import { usePathname, useRouter } from "next/navigation";
import { AiOutlineCar } from "react-icons/ai";
import { useParamsStore } from "../hooks/useParamsStore";

export default function Logo() {
    const router = useRouter();
    const pathname = usePathname();
    const reset = useParamsStore((state) => state.reset);

    function doReset() {
        if (pathname !== "/") {
            router.push("/");
            reset();
        }
    }
    return (
        <div className="flex-1">
            <div
                onClick={doReset}
                className="cursor-pointer flex items-center gap-2 text-3xl font-semibold"
            >
                <AiOutlineCar size={34} />
                <div>Carsties</div>
            </div>
        </div>
    );
}
