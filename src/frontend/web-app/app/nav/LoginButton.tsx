"use client";

import { signIn } from "next-auth/react";

export default function LoginButton() {
    return (
        <>
            <button
                className="btn"
                onClick={() => signIn("id-server", { callbackUrl: "/" })}
            >
                Login
            </button>
        </>
    );
}
