"use client";

import { useState } from "react";
import { updateAuctionTest } from "../actions/auctionActions";

export default function Auth() {
    const [loading, setLoading] = useState(false);
    const [result, setResult] = useState<any>();

    function doUpdate() {
        setResult(undefined); // Testing purpose
        setLoading(true);
        updateAuctionTest()
            .then((res) => setResult(res))
            .finally(() => setLoading(false));
    }

    return (
        <div className="flex items-center gap-4">
            <button className="btn" disabled={loading} onClick={doUpdate}>
                Test doUpdate
            </button>
            <div>{JSON.stringify(result, null, 2)}</div>
        </div>
    );
}
