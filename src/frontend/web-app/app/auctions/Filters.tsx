import { AiOutlineClockCircle, AiOutlineSortAscending } from "react-icons/ai";
import { BsFillStopCircleFill, BsStopwatchFill } from "react-icons/bs";
import { GiFinishLine, GiFlame } from "react-icons/gi";
import { useParamsStore } from "../hooks/useParamsStore";

const pageSizeButtons = [4, 8, 12];

const orderButtons = [
    {
        label: "Alphabetical",
        icon: AiOutlineSortAscending,
        value: "make",
    },
    {
        label: "End date",
        icon: AiOutlineClockCircle,
        value: "endingSoon",
    },
    {
        label: "Recently added",
        icon: BsFillStopCircleFill,
        value: "new",
    },
];

const filterButtons = [
    {
        label: "Live Auctions",
        icon: GiFlame,
        value: "live",
    },
    {
        label: "Ending < 6 hours",
        icon: GiFinishLine,
        value: "endingSoon",
    },
    {
        label: "Completed",
        icon: BsStopwatchFill,
        value: "finished",
    },
];

export default function Filters() {
    const pageSize = useParamsStore((state) => state.pageSize);
    const setParams = useParamsStore((state) => state.setParams);
    const orderBy = useParamsStore((state) => state.orderBy);
    const filterBy = useParamsStore((state) => state.filterBy);
    return (
        <div className="flex justify-between items-center mb-4">
            <div>
                <span className="uppercase text-sm text-gray-500 mr-2">
                    Filter by
                </span>
                <div className="join">
                    {filterButtons.map(({ label, icon: Icon, value }) => (
                        <button
                            className="btn join-item"
                            key={value}
                            onClick={() => setParams({ filterBy: value })}
                            color={`${filterBy === value ? "red" : "gray"}`}
                        >
                            <Icon className="mr-3 h-4 w-4" />
                            {label}
                        </button>
                    ))}
                </div>
            </div>
            <div>
                <span className="uppercase text-sm text-gray-500 mr-2">
                    Order by
                </span>
                <div className="join">
                    {orderButtons.map(({ label, icon: Icon, value }) => (
                        <button
                            className="btn join-item"
                            key={value}
                            onClick={() => setParams({ orderBy: value })}
                            color={`${orderBy === value ? "red" : "gray"}`}
                        >
                            <Icon className="mr-3 h-4 w-4" />
                            {label}
                        </button>
                    ))}
                </div>
            </div>

            <div>
                <span className="uppercase text-sm text-gray-500 mr-2">
                    Page size
                </span>
                <div className="join">
                    {pageSizeButtons.map((value, i) => (
                        <button
                            className="btn join-item"
                            key={i}
                            onClick={() => setParams({ pageSize: value })}
                            color={`${pageSize === value ? "red" : "gray"}`}
                        >
                            {value}
                        </button>
                    ))}
                </div>
            </div>
        </div>
    );
}
