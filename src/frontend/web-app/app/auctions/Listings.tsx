"use client";

import qs from "query-string";
import { useEffect, useState } from "react";
import { shallow } from "zustand/shallow";
import { getData } from "../actions/auctionActions";
import AppPagination from "../components/AppPagination";
import EmptyFilter from "../components/EmptyFilter";
import { useParamsStore } from "../hooks/useParamsStore";
import { Auction, PagedResult } from "../types";
import AuctionCard from "./AuctionCard";
import Filters from "./Filters";

export default function Listings() {
    const [data, setData] = useState<PagedResult<Auction>>();
    const params = useParamsStore(
        (state) => ({
            pageNumber: state.pageNumber,
            pageSize: state.pageSize,
            searchTerm: state.searchTerm,
            orderBy: state.orderBy,
            filterBy: state.filterBy,
        }),
        shallow // Re-compute and trigger a re-render when the selected properties actually change, rather on every state update
    );
    const setParams = useParamsStore((state) => state.setParams);
    const url = qs.stringifyUrl({ url: "", query: params });
    function setPageNumber(pageNumber: number) {
        setParams({ pageNumber });
    }
    useEffect(() => {
        getData(url).then((data) => {
            setData(data);
        });
    }, [url]);

    if (!data)
        return (
            <div className="flex h-screen">
                <div className="m-auto">
                    <span className="loading loading-infinity loading-lg"></span>
                </div>
            </div>
        );

    return (
        <>
            <Filters />
            {data.totalCount === 0 ? (
                <EmptyFilter showReset />
            ) : (
                <>
                    <div className="grid grid-cols-4 gap-6">
                        {data &&
                            data.results.map((auction: any) => (
                                <AuctionCard
                                    key={auction.id}
                                    auction={auction}
                                />
                            ))}
                    </div>
                    <div className="flex justify-center mt-4">
                        <AppPagination
                            pageChanged={setPageNumber}
                            currentPage={params.pageNumber}
                            pageCount={data.pageCount}
                        />
                    </div>
                </>
            )}
        </>
    );
}
