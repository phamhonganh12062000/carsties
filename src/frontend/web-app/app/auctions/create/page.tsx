import Heading from "@/app/components/Heading";
import AuctionForm from "../AuctionForm";

export default async function Create() {
    return (
        <div className="mx-auto max-w-[75%] shadow-lg p-10 rounded-lg">
            <Heading
                title="Sell your car!"
                subtitle="Please enter the details of your car"
            />
            <AuctionForm />
        </div>
    );
}
