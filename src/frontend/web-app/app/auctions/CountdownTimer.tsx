"use client";

import Countdown, { zeroPad } from "react-countdown";

type Props = {
    auctionEnd: string;
};

type CountdownProps = {
    days: number;
    hours: number;
    minutes: number;
    seconds: number;
    completed: boolean;
};

const renderer = ({
    days,
    hours,
    minutes,
    seconds,
    completed,
}: CountdownProps) => {
    return (
        <div
            className={`
          border-2
          border-white
          text-white py-1 px-2
          rounded-lg flex justify-center
          ${
              completed
                  ? "bg-red-600"
                  : days === 0 && hours < 10
                  ? "bg-amber-600"
                  : "bg-green-600"
          }
      `}
        >
            {completed ? (
                <span className="countdown font-mono text-1xl">
                    Auction finished
                </span>
            ) : (
                <span className="countdown font-mono text-2xl">
                    <span
                        style={
                            {
                                "--value": `${zeroPad(days)}`,
                            } as React.CSSProperties
                        }
                    ></span>
                    d
                    <span
                        style={
                            {
                                "--value": `${zeroPad(hours)}`,
                            } as React.CSSProperties
                        }
                    ></span>
                    h
                    <span
                        style={
                            {
                                "--value": `${zeroPad(minutes)}`,
                            } as React.CSSProperties
                        }
                    ></span>
                    m
                    <span
                        style={
                            {
                                "--value": `${zeroPad(seconds)}`,
                            } as React.CSSProperties
                        }
                    ></span>
                    s
                </span>
            )}
        </div>
    );
};

export default function CountdownTimer({ auctionEnd }: Props) {
    return (
        <div>
            <Countdown date={auctionEnd} renderer={renderer} />
        </div>
    );
}
