"use client";

import { Auction } from "@/app/types";

type Props = {
    auction: Auction;
};

export default function DetailedSpecs({ auction }: Props) {
    return (
        <div className="overflow-x-auto">
            <table className="table">
                {/* head */}
                <thead>
                    <tr>
                        <th></th>
                        <th>Specification</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    {/* Seller row */}
                    <tr>
                        <th>1</th>
                        <td>Seller</td>
                        <td>{auction.seller}</td>
                    </tr>
                    {/* Make row */}
                    <tr>
                        <th>2</th>
                        <td>Make</td>
                        <td>{auction.make}</td>
                    </tr>
                    {/* Model row */}
                    <tr>
                        <th>3</th>
                        <td>Model</td>
                        <td>{auction.model}</td>
                    </tr>
                    {/* Year manufactured row */}
                    <tr>
                        <th>4</th>
                        <td>Year manufactured</td>
                        <td>{auction.year}</td>
                    </tr>
                    {/* Mileage row */}
                    <tr>
                        <th>5</th>
                        <td>Mileage</td>
                        <td>{auction.mileage}</td>
                    </tr>
                    {/* Reserve price row */}
                    <tr>
                        <th>6</th>
                        <td>Has reserve price?</td>
                        <td>{auction.reservePrice > 0 ? "Yes" : "No"}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}
