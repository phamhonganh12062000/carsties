"use client";

import Link from "next/link";

type Props = {
    id: string;
};

export default function EditButton({ id }: Props) {
    return (
        <button className="btn btn-outline btn-primary">
            <Link href={`/auctions/update/${id}`}>Update Auction</Link>
        </button>
    );
}
