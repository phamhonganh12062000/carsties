import Link from "next/link";
import CarImage from "./CarImage";
import CountdownTimer from "./CountdownTimer";
import { Auction } from "../types";

type Props = {
    auction: Auction;
};

export default function AuctionCard({ auction }: Props) {
    return (
        <Link href={`/auctions/details/${auction.id}`} className="group">
            <div className="card shadow-xl">
                <div className="w-full bg-base-100 aspect-w-16 aspect-h-10 rounded-lg overflow-hidden">
                    <figure>
                        <CarImage imageUrl={auction.imageUrl} />
                        <div className="absolute bottom-2 left-2">
                            <CountdownTimer auctionEnd={auction.auctionEnd} />
                        </div>
                    </figure>
                </div>
                <div className="card-body">
                    <h2 className="card-title">
                        {auction.make} {auction.model}
                    </h2>
                    <p>{auction.year}</p>
                    <div className="card-actions justify-end">
                        <div className="badge badge-outline">Fashion</div>
                        <div className="badge badge-outline">Products</div>
                    </div>
                </div>
            </div>
        </Link>
    );
}
