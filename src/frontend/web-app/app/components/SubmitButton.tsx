type Props = {
    isSubmitting: boolean;
    isValid: boolean;
};

export default function SubmitButton({ isSubmitting, isValid }: Props) {
    return (
        <button
            className="btn btn-primary"
            disabled={isSubmitting || !isValid}
            type="submit"
            color="success"
        >
            Submit
        </button>
    );
}
