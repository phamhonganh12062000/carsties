import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useController, UseControllerProps } from "react-hook-form";

type Props = {
    label: string;
    type?: string;
    showLabel?: boolean;
} & UseControllerProps & // Add props of UseControllerProps to props
    Partial<ReactDatePickerProps>; // (Optionally) add props of ReactDatePickerProps to props

export default function DateInput(props: Props) {
    const { fieldState, field } = useController({ ...props, defaultValue: "" });

    return (
        <div className="block">
            <DatePicker
                {...props}
                {...field}
                onChange={(value) => field.onChange(value)}
                selected={field.value}
                placeholderText={props.label}
                className={`
                        input input-bordered w-full
                        ${
                            fieldState.error
                                ? "input-error"
                                : !fieldState.invalid && fieldState.isDirty
                                ? "input-success"
                                : ""
                        }
                    `}
            />
            {fieldState.error && (
                <div className="text-red-500 text-sm">
                    {fieldState.error.message}
                </div>
            )}
        </div>
    );
}
