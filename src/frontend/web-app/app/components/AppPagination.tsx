"use client";

type Props = {
    currentPage: number;
    pageCount: number;
    pageChanged: (page: number) => void;
};

export default function AppPagination({
    currentPage,
    pageCount,
    pageChanged,
}: Props) {
    const handlePrevClick = () => {
        if (currentPage > 1) {
            pageChanged(currentPage - 1);
        }
    };

    const handleNextClick = () => {
        if (currentPage < pageCount) {
            pageChanged(currentPage + 1);
        }
    };

    // const renderPageNumbers = () => {
    //     const pageNumbers = [];

    //     for (let i = 1; i <= pageCount; i++) {
    //         pageNumbers.push(
    //             <button
    //                 key={i}
    //                 onClick={() => pageChanged(i)}
    //                 disabled={i === currentPage}
    //             >
    //                 {i}
    //             </button>
    //         );
    //     }

    //     return pageNumbers;
    // };

    return (
        <div className="join">
            <button
                className="join-item btn"
                onClick={handlePrevClick}
                disabled={currentPage === 1}
            >
                «
            </button>
            <button className="join-item btn">{currentPage}</button>
            <button
                className="join-item btn"
                onClick={handleNextClick}
                disabled={currentPage === pageCount}
            >
                »
            </button>
        </div>
    );
}
