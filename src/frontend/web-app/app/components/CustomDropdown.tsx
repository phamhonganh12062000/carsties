"use client";

type Props = {
    label: string;
    children: any;
};

export default function CustomDropdown({ label, children }: Props) {
    return (
        <div className="dropdown">
            <div tabIndex={0} role="button" className="btn m-1">
                {label}
            </div>
            <ul
                tabIndex={0}
                className="menu menu-sm dropdown-content bg-base-100 rounded-box z-[1] mt-3 w-52 p-2 shadow"
            >
                {children}
            </ul>
        </div>
    );
}
