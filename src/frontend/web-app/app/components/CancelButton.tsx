"use client";

import { useRouter } from "next/navigation";

export default function CancelButton() {
    const router = useRouter();

    const handleCancel = () => {
        router.back();
    };
    return (
        <button className="btn btn-active btn-neutral" onClick={handleCancel}>
            Cancel
        </button>
    );
}
