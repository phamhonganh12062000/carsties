import { useController, UseControllerProps } from "react-hook-form";

type Props = {
    label: string;
    type?: string;
    showLabel?: boolean;
} & UseControllerProps;

export default function CustomInput(props: Props) {
    const { fieldState, field } = useController({ ...props, defaultValue: "" });
    return (
        <div className="mb-3">
            {props.showLabel && (
                <div className="mb-2 block">
                    <label
                        htmlFor={field.name}
                        className="form-control w-full max-w-xs"
                    >
                        <div className="label">
                            <span className="label-text">{props.label}</span>
                        </div>
                        <div className="label">
                            <span className="label-text-alt">
                                {fieldState.error?.message}
                            </span>
                        </div>
                    </label>
                </div>
            )}
            <input
                type={props.type || "text"}
                placeholder={props.label}
                className="input input-bordered w-full"
                {...props}
                {...field}
                color={
                    fieldState.error
                        ? "failure"
                        : !fieldState.isDirty
                        ? ""
                        : "success"
                }
            />
        </div>
    );
}
