"use client";

import { signIn } from "next-auth/react";
import { useParamsStore } from "../hooks/useParamsStore";
import Heading from "./Heading";

type Props = {
    title?: string;
    subtitle?: string;
    showReset?: boolean;
    showLogin?: boolean
    callbackUrl?: string
};

export default function EmptyFilter({
    title = "No matches for this filter",
    subtitle = "Try changing or resetting the filter",
    showReset,
    showLogin,
    callbackUrl
}: Props) {
    const reset = useParamsStore((state) => state.reset);

    return (
        <div className="h-[40vh] flex flex-col gap-2 justify-center items-center shadow-lg">
            <Heading title={title} subtitle={subtitle} center />
            <div className="mt-4">
                {showReset && <button onClick={reset}>Remove Filters</button>}
                {showLogin && (
                    <button onClick={() => signIn('id-server', {callbackUrl})}>Login</button>
                )}
            </div>
        </div>
    );
}
