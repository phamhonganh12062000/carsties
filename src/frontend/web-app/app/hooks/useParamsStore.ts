import { create } from "zustand";

type State = {
    pageNumber: number;
    pageSize: number;
    pageCount: number;
    searchTerm: string;
    searchValue: string;
    orderBy: string;
    filterBy: string;
    seller?: string;
    winner?: string;
};

type Actions = {
    setParams: (params: Partial<State>) => void;
    reset: () => void;
    setSearchValue: (value: string) => void;
};

const initialState: State = {
    pageNumber: 1,
    pageSize: 12,
    pageCount: 1,
    searchTerm: "",
    searchValue: "",
    orderBy: "make",
    filterBy: "live",
    seller: undefined,
    winner: undefined,
};

// Specify the shape of the state and actions objects
export const useParamsStore = create<State & Actions>()((set) => ({
    ...initialState, // Spread the initial state obj to set the initial values

    // Update the state based on the provided params
    setParams: (newParams: Partial<State>) => {
        set((state) => {
            if (newParams.pageNumber) {
                return { ...state, pageNumber: newParams.pageNumber };
            } else {
                return { ...state, ...newParams, pageNumber: 1 };
            }
        });
    },

    // Reset the state
    reset: () => set(initialState),

    // Update the searchValue only
    setSearchValue: (value: string) => {
        set({ searchValue: value });
    },
}));
