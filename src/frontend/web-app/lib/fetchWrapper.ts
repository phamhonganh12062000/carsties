import { getTokenWorkaround } from "@/app/actions/authActions";

const baseUrl = "http://localhost:6001/";

async function getHeaders() {
    const token = await getTokenWorkaround();
    const headers = { "Content-type": "application/json" } as any;
    if (token) {
        headers.Authorization = "Bearer " + token.access_token;
    }

    return headers;
}

async function get(endpoint: string) {
    const requestOptions = {
        method: "GET",
        header: await getHeaders(),
    };

    const response = await fetch(baseUrl + endpoint, requestOptions);

    return await handleResponse(response);
}

async function post(endpoint: string, body: {}) {
    const requestOptions = {
        method: "POST",
        headers: await getHeaders(),
        body: JSON.stringify(body),
    };

    const response = await fetch(baseUrl + endpoint, requestOptions);

    return await handleResponse(response);
}

async function put(endpoint: string, body: {}) {
    const requestOptions = {
        method: "PUT",
        headers: await getHeaders(),
        body: JSON.stringify(body),
    };

    const response = await fetch(baseUrl + endpoint, requestOptions);

    return await handleResponse(response);
}

async function del(endpoint: string) {
    const requestOptions = {
        method: "DELETE",
        headers: await getHeaders(),
    };

    const response = await fetch(baseUrl + endpoint, requestOptions);

    return await handleResponse(response);
}

async function handleResponse(response: Response) {
    const text = await response.text();
    const data = text && JSON.parse(text);

    if (response.ok) {
        return data || response.statusText;
    } else {
        const error = {
            status: response.status,
            message: response.statusText,
        };

        console.log(error);
        return { error };
    }
}

export const fetchWrapper = {
    get,
    post,
    put,
    del,
};
